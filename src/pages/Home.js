import { Fragment, useContext, useEffect } from "react";
import CarouselApp from "../components/Carousel";
import Search from "../components/Search";
import Tabs from "../components/Tabs";
import UserContext from "../UserContext";
// import Swal from "sweetalert2";
import Admin from "../components/Admin";
import Swal from "sweetalert2";
// import { Container } from "react-bootstrap";

export default function Home() {
  const {
    user,
    setUser,
    setLastPage,
    checkUser,
    setCheckUser,
    setCheckAdminUpdate,
  } = useContext(UserContext);

  useEffect(() => {
    if (localStorage.getItem("token") !== null && !checkUser) {
      if (user.isAdmin) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          // backdrop: "rgba(255, 255, 255, 0.0)",
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
      }
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            userId: data.userId,
            firstName: data.firstName,
            lastName: data.lastName,
            mobileNumber: data.mobileNumber,
            email: data.email,
            isAdmin: data.isAdmin,
            folderId: data.folderId,
            fileId: data.fileId,
            profileImage: data.profileImage,
            token: localStorage.getItem("token"),
            cart: data.cart,
          });
          setCheckUser(true);
          if (user.isAdmin) Swal.close();
        });
    }

    setLastPage("/");
  }, [
    setLastPage,
    setCheckAdminUpdate,
    checkUser,
    setUser,
    setCheckUser,
    user.isAdmin,
  ]);
  return (
    <Fragment>
      {!user.isAdmin ? (
        <Fragment>
          <div className="d-md-none mb-3 mx-3">
            <Search />
          </div>
          <CarouselApp />
          <Tabs />
        </Fragment>
      ) : (
        <Admin id="admin" />
      )}
    </Fragment>
  );
}
