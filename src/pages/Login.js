import {
  Button,
  Card,
  Row,
  Col,
  Container,
  Form,
  InputGroup,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import { FaEnvelope, FaUnlock, FaEyeSlash, FaEye } from "react-icons/fa";
import { useContext, useState } from "react";
import Tilt from "react-parallax-tilt";
import { Link, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login() {
  const { user, setUser, lastPage } = useContext(UserContext);

  const [passType, setPassType] = useState("password");
  const [focused, setFocused] = useState({
    email: "false",
    password: "false",
  });
  const [values, setValues] = useState({
    email: "",
    password: "",
  });
  const inputs = [
    {
      id: 1,
      name: "email",
      type: "email",
      placeholder: "Email",
      errorMessage: "It should be a valid email address!",
      icon: FaEnvelope,
      required: true,
    },
    {
      id: 2,
      name: "password",
      type: "password",
      placeholder: "Password",
      errorMessage:
        "Password should be 8-20 characters, at least 1 letter, 1 number and 1 special character!",
      icon: FaUnlock,
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
  ];

  function forgotPassword() {
    Swal.fire({
      title: "Forgot your password?",
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <p>Please enter the email address associated with your account and We will email you a link to reset your password. Please, check your spam folder as well.</p>
                <label for="email" style="font-size:16px;text-align:left !important;">Email Address</label>
                <input id="email" type="email" class="form-control mb-2" placeholder="email@example.com" >
              </div>
                `,
      confirmButtonText: "Reset Password",
      showDenyButton: false,
      focusConfirm: false,
      preConfirm: () => {
        const email = Swal.getPopup().querySelector("#email").value;
        if (!validateEmailAddress(email)) {
          Swal.showValidationMessage(`Please input valid email`);
        }

        return {
          email: email,
        };
      },
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        fetch(`${process.env.REACT_APP_API_URL}/users/forgotPassword`, {
          method: "PUT",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            email: result.value.email,
          }),
        })
          .then((res) => res.json())
          .then(
            (resultUpdate) =>
              new Promise((resolve, reject) => {
                if (resultUpdate.accepted[0] === result.value.email) {
                  Swal.fire({
                    title: "Password Reset Link Sent",
                    icon: "success",
                    iconColor: "#44476a",
                    background: "#e6e7ee",
                    text: "Please check your email to reset your password, Thank you!",
                  });
                }
              })
          )
          .catch((error) => {
            Swal.fire({
              title: "Password Reset Not Successful",
              icon: "error",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Something went wrong, reset link expired.",
            });
          });
      }
    });
  }

  function loginUser(e) {
    Swal.fire({
      allowEscapeKey: false,
      allowOutsideClick: false,
      showConfirmButton: false,
      customClass: {
        popup: "my-swal",
        loader: "my-swal-loader",
      },
      willOpen: () => {
        Swal.showLoading();
      },
    });
    e.preventDefault();
    setFocused({
      email: "true",
      password: "true",
    });
    if (
      ((values.email === "" || !validateEmailAddress(values.email)) &&
        values.password === "") ||
      !validatePassword(values.password)
    ) {
      Swal.fire({
        title: "Input validation failed",
        icon: "error",
        iconColor: "#44476a",
        text: "Check your login credentials and try again.",
      });
    } else {
      fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          email: values.email,
          password: values.password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (typeof data.access !== "undefined") {
            localStorage.setItem("token", data.access);

            setUser({
              userId: data.userId,
              firstName: data.firstName,
              lastName: data.lastName,
              mobileNumber: data.mobileNumber,
              email: data.email,
              isAdmin: data.isAdmin,
              folderId: data.profileImage.folderId,
              fileId: data.profileImage.fileId,
              profileImage: data.profileImage.link,
              token: data.access,
              cart: data.cart,
            });

            // Swal.fire({
            //   title: "Login successful!",
            //   icon: "success",
            //   iconColor: "#44476a",
            //   background: "#e6e7ee",
            //   text: `Welcome to Dodoflorie! ${data.email}`,
            // });
          } else {
            Swal.fire({
              title: "Authentication Failed",
              icon: "error",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Check your login credentials and try again.",
            });
          }
        });
    }
  }

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const handleFocus = (e) => {
    setFocused({ ...focused, [e.target.name]: "true" });
  };
  const toggleShowPassword = (e) => {
    e.preventDefault();
    if (passType === "password") setPassType("text");
    else setPassType("password");
  };

  function validateEmailAddress(input) {
    let regex = /[^\s@]+@[^\s@]+\.[^\s@]+/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }
  function validatePassword(input) {
    let regex =
      /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  return user.userId !== null ? (
    <Navigate to={lastPage} />
  ) : (
    <Container className="min-vh-100 d-flex bg-primary align-items-center pb-4">
      <Card className="bg-primary shadow-soft border-light p-3">
        <Row className="justify-content-around">
          <Col
            md={6}
            lg={5}
            className="card justify-content-center bg-primary shadow-inset border-light m-3"
          >
            <Tilt className="Tilt" options={{ scale: 0.9 }}>
              <img
                src="https://lh3.googleusercontent.com/d/1sfrtqi2LEndGBiCSr0n-UAVnKp6pW3XL"
                alt="PNG"
              ></img>
            </Tilt>
          </Col>
          <Col md={6} lg={5} className="justify-content-center">
            <Card.Header className="text-center pb-5">
              <h2 className="h4">User's Login</h2>
            </Card.Header>
            <Form onSubmit={(e) => loginUser(e)} noValidate className="p-2">
              {inputs.map((input) => (
                <Form.Group className="pb-4" key={input.id}>
                  <InputGroup>
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <input.icon />
                      </span>
                    </div>
                    <Form.Control
                      className="rounded-right-side"
                      name={input.name}
                      type={input.name === "email" ? input.type : passType}
                      placeholder={input.placeholder}
                      value={values[input.name]}
                      onChange={onChange}
                      required={input.required}
                      pattern={input.pattern}
                      onBlur={handleFocus}
                      focused={focused[input.name]}
                      autoComplete="true"
                    />
                    {input.name === "password" ? (
                      <span id="showPass">
                        {passType === "password" ? (
                          <FaEyeSlash onClick={toggleShowPassword}></FaEyeSlash>
                        ) : (
                          <FaEye onClick={toggleShowPassword}></FaEye>
                        )}{" "}
                      </span>
                    ) : (
                      <></>
                    )}
                    <OverlayTrigger
                      placement="top"
                      overlay={<Tooltip>{input.errorMessage}</Tooltip>}
                    >
                      <span id="errorMsg"></span>
                    </OverlayTrigger>
                  </InputGroup>
                </Form.Group>
              ))}
              <Button
                className="btn btn-block btn-primary mt-4"
                type="submit"
                id="submitBtn"
              >
                Log in
              </Button>
            </Form>
            <div className="mt-2 text-center">
              <div>
                <span
                  className="small text-right"
                  onClick={forgotPassword}
                  style={{ cursor: "pointer" }}
                >
                  {" "}
                  Forgot password?
                </span>
              </div>
            </div>
            <div className="d-flex justify-content-center align-items-center mt-5">
              <span className="font-weight-normal">
                Not registered?
                <Link to="/register" className="font-weight-bold">
                  {" "}
                  Create account
                </Link>
              </span>
            </div>
          </Col>
        </Row>
      </Card>
    </Container>
  );
}
