import { useContext, useEffect, useState } from "react";
import { Card, Container, Table } from "react-bootstrap";
import { IoRefreshCircleOutline } from "react-icons/io5";
import Search from "../components/Search";
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function OrderHistory() {
  const [orders, setOrders] = useState([]);
  const { user, setCheckAdminUpdate, updateOrderList, setUpdateOrderList } =
    useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!updateOrderList) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(`${process.env.REACT_APP_API_URL}/users/order/purchased`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setOrders(data);
          setUpdateOrderList(true);
          Swal.close();
        });
    }
    if (user.userId === null) navigate("/");
  }, [user, updateOrderList, setUpdateOrderList, navigate]);

  function refreshProductList() {
    setCheckAdminUpdate(false);
    setUpdateOrderList(false);
  }

  return (
    <>
      <div className="d-md-none mb-3  mx-3">
        <Search />
      </div>
      <Container fluid className="text-center adminStyle py-3">
        <h1 className="mb-3 text-gray">Order History</h1>
        <Card className="shadow-soft">
          <Container
            fluid
            style={{
              overflowX: "auto",
              overflowY: "auto",
              margin: "0",
              minHeight: "45vh",
              maxHeight: "80vh",
            }}
            className="scrollEdit"
          >
            <Table striped className="rounded mt-2">
              {/* style={{ userSelect: "none" }}> */}
              <thead>
                <tr>
                  <th style={{ width: "calc(100%/4)" }} className="border-0">
                    Date{" "}
                    <IoRefreshCircleOutline
                      className="shadow-inset rounded"
                      style={{ cursor: "pointer" }}
                      onClick={() => refreshProductList()}
                    />
                  </th>
                  <th style={{ width: "calc(100%/4)" }} className="border-0">
                    Name
                  </th>
                  <th style={{ width: "calc(100%/8)" }} className="border-0">
                    Price
                  </th>
                  <th style={{ width: "calc(100%/8)" }} className="border-0">
                    Quantity
                  </th>
                  <th style={{ width: "calc(100%/4)" }} className="border-0">
                    Sub Total
                  </th>
                </tr>
              </thead>
              <tbody>
                {orders.isSuccess ? (
                  orders.purchasedOrder.purchasedOrderProducts.map((order) => (
                    <tr key={order.productId + order.transactionDate}>
                      <td className="border-0">{order.transactionDate}</td>
                      <td className="border-0">{order.name}</td>
                      <td className="border-0">₱{order.price}</td>
                      <td className="border-0">{order.quantity}</td>
                      <td className="border-0">
                        ₱{order.price * order.quantity}
                      </td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td colSpan={5}>no purchased yet.</td>
                  </tr>
                )}
                {orders.isSuccess ? (
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <th>Total</th>
                    <td>{orders.purchasedOrder.totalAmount}</td>
                  </tr>
                ) : (
                  <></>
                )}
              </tbody>
            </Table>
          </Container>
        </Card>
      </Container>
    </>
  );
}
