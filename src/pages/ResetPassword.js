import Swal from "sweetalert2";
import { useNavigate, useParams } from "react-router-dom";
export default function ResetPassword() {
  const navigate = useNavigate();
  const { resetToken } = useParams();

  Swal.fire({
    title: "Reset Password",
    html: ` <div class="form-group px-3 pt-3 px-md-5" >
              <label for="newPassword" style="font-size:16px">New Password</label>
              <input id="newPassword" type="password" class="form-control mb-2" >
              <label for="confirmNewPassword" style="font-size:16px">Confirm New Password</label>
              <input id="confirmNewPassword" type="password" class="form-control mb-2" >
              <input id="swal2-checkbox" type="checkbox" > 
              <label for="swal2-checkbox" style="font-size:16px">Show Password</label>
            </div>
              `,
    confirmButtonText: "Save",
    showDenyButton: true,
    denyButtonText: "No",
    allowEscapeKey: false,
    allowOutsideClick: false,
    focusConfirm: false,
    didOpen: (e) => {
      const checkBox = Swal.getPopup().querySelector("#swal2-checkbox");
      const newPass = Swal.getPopup().querySelector("#newPassword");
      const curNewPass = Swal.getPopup().querySelector("#confirmNewPassword");

      let clickOnce = false;

      checkBox.addEventListener("click", function () {
        if (clickOnce === false) {
          newPass.type = "text";
          curNewPass.type = "text";
          clickOnce = true;
        } else {
          newPass.type = "password";
          curNewPass.type = "password";
          clickOnce = false;
        }
      });
    },
    preConfirm: () => {
      const newPassword = Swal.getPopup().querySelector("#newPassword").value;
      const confirmNewPassword = Swal.getPopup().querySelector(
        "#confirmNewPassword"
      ).value;
      if (
        !validatePassword(newPassword) ||
        !validatePassword(confirmNewPassword) ||
        confirmNewPassword !== newPassword
      ) {
        Swal.showValidationMessage(`Please check your password`);
      }

      return {
        newPassword: newPassword,
      };
    },
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(`${process.env.REACT_APP_API_URL}/users/resetPassword`, {
        method: "PUT",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          resetToken: resetToken,
          newPassword: result.value.newPassword,
        }),
      })
        .then((res) => res.json())
        .then(
          (resultUpdate) =>
            new Promise((resolve, reject) => {
              if (resultUpdate.isSuccess) {
                Swal.fire({
                  title: "Congratulations!",
                  icon: "success",
                  iconColor: "#44476a",
                  background: "#e6e7ee",
                  text: "password reset successful!",
                });
              } else {
                Swal.fire({
                  title: "Password Reset Failed",
                  icon: "error",
                  iconColor: "#44476a",
                  background: "#e6e7ee",
                  text: "Something went wrong, try again later.",
                });
              }
              navigate("/login");
            })
        )
        .catch((error) => {
          Swal.fire({
            title: "Password Reset Failed",
            icon: "error",
            iconColor: "#44476a",
            background: "#e6e7ee",
            text: "Something went wrong, try again later.",
          });
          navigate("/login");
        });
    }
    if (result.isDenied) {
      navigate("/login");
    }
  });

  function validatePassword(input) {
    let regex =
      /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  return <></>;
}
