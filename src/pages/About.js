import { Container, Card, Row, Col } from "react-bootstrap";
import Search from "../components/Search";
import Tilt from "react-parallax-tilt";

export default function About() {
  return (
    <>
      <div className="d-md-none mb-3  mx-3">
        <Search />
      </div>
      <Container
        className="text-center adminStyle py-3"
        style={{ minHeight: "55vh" }}
      >
        <h1 className="mb-3 text-gray">About</h1>
        <Card className="shadow-inset">
          <Row className="justify-content-around">
            <Col md={6} lg={5} className="p-5">
              <Tilt options={{ scale: 0.9 }}>
                <div className="m-2 p-1 shadow-soft organic-mod-radius">
                  <img
                    src="https://lh3.googleusercontent.com/d/1lrCNxBayxwmnV2Lj21lrAtO4tltm85Fp"
                    className="organic-mod-radius p-2"
                    style={{ width: "100%", objectFit: "contain" }}
                    alt="PNG"
                  ></img>
                </div>
              </Tilt>
            </Col>
            <Col md={6} lg={5} className="justify-content-center">
              <div className="p-3">
                <h2 className="h2 mt-3 text-gray">DodoFlorie</h2>
                <h6 className="h6 mt-1 text-gray mb-5">
                  General Merchandise, Eatery and Rice Mill
                </h6>
                <p>
                  Since 1990, — a family owned independent store. The owner was
                  a marine engineer and started the store to go along when he is
                  on vacation. He retired to focus on the store, and uses the
                  knowledge from his old days to help customers with their home
                  repairs.
                </p>
                <p>
                  Old-fashioned community store packed with friendly advice and
                  goods for the entire home. Located conveniently on the
                  highway.
                </p>
              </div>
            </Col>
          </Row>
        </Card>
        <Card className="mt-5">
          <h3 className="h2 mt-3 mb-3">Location map</h3>
          <div className="d-flex justify-content-center">
            <iframe
              className="shadow-soft p-1"
              title="location"
              id="gmap_canvas"
              src="https://maps.google.com/maps?q=DODO%20FLORIE%20MERCHANDISE&t=&z=13&ie=UTF8&iwloc=&output=embed"
              frameBorder={0}
              scrolling="no"
              marginHeight={0}
              marginWidth={0}
              width="75%"
              height="400px"
            ></iframe>
          </div>
        </Card>
      </Container>
    </>
  );
}
