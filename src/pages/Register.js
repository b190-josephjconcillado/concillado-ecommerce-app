import {
  Button,
  Card,
  Row,
  Col,
  Container,
  Form,
  InputGroup,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import { FaEnvelope, FaUnlock, FaEyeSlash, FaEye } from "react-icons/fa";
import { Fragment, useContext, useState } from "react";
import { Link, useNavigate, Navigate } from "react-router-dom";
import Tilt from "react-parallax-tilt";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [passType, setPassType] = useState("password");
  const [focused, setFocused] = useState({
    email: "false",
    password: "false",
    confirmPassword: "false",
  });
  const [values, setValues] = useState({
    email: "",
    password: "",
    confirmPassword: "",
  });
  const inputs = [
    {
      id: 1,
      name: "email",
      type: "email",
      placeholder: "Email",
      errorMessage: "It should be a valid email address!",
      icon: FaEnvelope,
      required: true,
    },
    {
      id: 2,
      name: "password",
      type: "password",
      placeholder: "Password",
      errorMessage:
        "Password should be 8-20 characters, at least 1 letter, 1 number and 1 special character!",
      icon: FaUnlock,
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
    {
      id: 3,
      name: "confirmPassword",
      type: "password",
      placeholder: "Confirm password",
      errorMessage: "Password does not matched!",
      icon: FaUnlock,
      pattern: values.password,
      required: true,
    },
  ];

  async function registerUser(e) {
    e.preventDefault();
    setFocused({
      email: "true",
      password: "true",
      confirmPassword: "true",
    });
    if (
      values.email !== "" &&
      validateEmailAddress(values.email) &&
      values.password !== "" &&
      validatePassword(values.password) &&
      values.confirmPassword !== "" &&
      validateConfirmPassword(values.confirmPassword)
    ) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      let checkUser = await fetch(
        `${process.env.REACT_APP_API_URL}/users/check`,
        {
          method: "POST",
          headers: {
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            email: values.email,
          }),
        }
      )
        .then((res) => res.json())
        .then((result) => {
          return result.isSuccess;
        });

      if (checkUser) {
        let folderId = await fetch(process.env.REACT_APP_GOOGLE_URL, {
          method: "POST",
          body: JSON.stringify({
            dataReq: {
              email: values.email,
              folderId: process.env.REACT_APP_GOOGLE_FOLDER_ID,
            },
            fname: "createUserFolder",
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            return data.id;
          });

        if (folderId !== null || typeof folderId !== "undefined") {
          let regResult = await fetch(
            `${process.env.REACT_APP_API_URL}/users/register`,
            {
              method: "POST",
              headers: {
                "Content-type": "application/json",
              },
              body: JSON.stringify({
                email: values.email,
                password: values.password,
                folderId: folderId,
              }),
            }
          )
            .then((res) => res.json())
            .then((result) => {
              return result.isSuccess;
            });

          if (regResult) {
            // Swal.close();
            Swal.fire({
              title: "Registration successful",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Thank you for joining us.",
            });
            navigate("/login");
          } else {
          }
        }
      } else {
        // Swal.close();
        Swal.fire({
          title: "Duplicate email found.",
          icon: "error",
          iconColor: "#44476a",
          background: "#e6e7ee",
          text: "Please provide different email.",
        });
      }
    } else {
      Swal.fire({
        title: "Input validation failed",
        icon: "error",
        iconColor: "#44476a",
        background: "#e6e7ee",
        text: "Check input field and try again.",
      });
    }
  }

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const handleFocus = (e) => {
    setFocused({ ...focused, [e.target.name]: "true" });
  };
  const toggleShowPassword = (e) => {
    e.preventDefault();
    if (passType === "password") setPassType("text");
    else setPassType("password");
  };

  function validateEmailAddress(input) {
    let regex = /[^\s@]+@[^\s@]+\.[^\s@]+/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  function validatePassword(input) {
    let regex =
      /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }
  function validateConfirmPassword(input) {
    if (values.password === values.confirmPassword) return true;
    else return false;
  }

  return user.userId !== null ? (
    <Navigate to="/" />
  ) : (
    <Fragment>
      <Container className="min-vh-100 d-flex bg-primary align-items-center pb-4">
        <Card className="bg-primary shadow-soft border-light p-3">
          <Row className="justify-content-around">
            <Col
              md={6}
              lg={5}
              className="card justify-content-center bg-primary shadow-inset border-light m-3"
            >
              <Tilt className="Tilt" options={{ scale: 0.9 }}>
                <img
                  src="https://lh3.googleusercontent.com/d/1sfrtqi2LEndGBiCSr0n-UAVnKp6pW3XL"
                  alt="PNG"
                ></img>
              </Tilt>
            </Col>
            <Col md={6} lg={5} className="justify-content-center">
              <Card.Header className="text-center pb-3">
                <h2 className="h4">Create Account</h2>
              </Card.Header>
              <Form
                onSubmit={(e) => registerUser(e)}
                noValidate
                className="p-2"
              >
                {inputs.map((input) => (
                  <Form.Group className="pb-4" key={input.id}>
                    <InputGroup>
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <input.icon />
                        </span>
                      </div>
                      <Form.Control
                        className="rounded-right-side"
                        name={input.name}
                        type={input.name === "email" ? input.type : passType}
                        placeholder={input.placeholder}
                        value={values[input.name]}
                        onChange={onChange}
                        required={input.required}
                        pattern={input.pattern}
                        onBlur={handleFocus}
                        focused={focused[input.name]}
                        autoComplete="true"
                      />
                      {input.name === "password" ? (
                        <span id="showPass">
                          {passType === "password" ? (
                            <FaEyeSlash
                              onClick={toggleShowPassword}
                            ></FaEyeSlash>
                          ) : (
                            <FaEye onClick={toggleShowPassword}></FaEye>
                          )}{" "}
                        </span>
                      ) : (
                        <></>
                      )}
                      <OverlayTrigger
                        placement="top"
                        overlay={<Tooltip>{input.errorMessage}</Tooltip>}
                      >
                        <span id="errorMsg"></span>
                      </OverlayTrigger>
                    </InputGroup>
                  </Form.Group>
                ))}
                <Button
                  className="btn btn-block btn-primary mt-4 mb-3"
                  type="submit"
                  id="submitBtn"
                >
                  Register
                </Button>
              </Form>

              <div className="d-block text-center d-flex justify-content-center align-items-center mt-4">
                <span className="font-weight-normal">
                  Already have an account?
                  <Link to="/login" className="font-weight-bold">
                    {" "}
                    Login here
                  </Link>
                </span>
              </div>
            </Col>
          </Row>
        </Card>
      </Container>
    </Fragment>
  );
}
