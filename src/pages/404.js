import { Link } from "react-router-dom";
import { Col, Row } from "react-bootstrap";

export default function ErrorPage() {
  return (
    <Row className="min-vh-100 d-flex bg-primary justify-content-center align-items-center pb-4 text-center">
      <Col sm={12} className="neumorph">
        <h1>404</h1>
        <h3>Page Not Found</h3>
        <p>
          Go back to <Link to="/">homepage</Link>
        </p>
      </Col>
      <Col sm={12}></Col>
    </Row>
  );
}
