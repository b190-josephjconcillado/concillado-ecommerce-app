import { useContext, useEffect, useState } from "react";
import { Button, Card, Container, Table } from "react-bootstrap";
import { IoRefreshCircleOutline } from "react-icons/io5";
import Search from "../components/Search";
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";
import CartValue from "../components/CartValue";
import { Cod, CreditDebit, GCash, Paypal } from "../components/HtmlCheckout";
import Swal from "sweetalert2";

export default function Cart() {
  const [orders, setOrders] = useState([]);
  const {
    setLastPage,
    checkUser,
    setUser,
    setCheckUser,
    user,
    setCheckAdminUpdate,
    updateOrderList,
    setUpdateOrderList,
  } = useContext(UserContext);
  const navigate = useNavigate();
  const [totalAmount, setTotalAmount] = useState(0);
  const [paymentMode, setPaymentMode] = useState({
    confirmButtonText: "Confirm",
    html: CreditDebit,
  });
  useEffect(() => {
    if (!updateOrderList) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(`${process.env.REACT_APP_API_URL}/users/order/pending`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.isSuccess) {
            setTotalAmount(data.pendingOrder.totalAmount);
            setOrders(
              data.pendingOrder.pendingOrderProducts.map((order) => {
                return (
                  <CartValue
                    key={order.productId + order.transactionDate}
                    cartProp={order}
                  />
                );
              })
            );
          } else {
            setTotalAmount(0);
          }
          setUpdateOrderList(true);
          Swal.close();
        });
    }
    if (user.userId === null) navigate("/");
  }, [paymentMode, user, updateOrderList, setUpdateOrderList, navigate]);

  useEffect(() => {
    if (localStorage.getItem("token") !== null && !checkUser) {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            userId: data.userId,
            firstName: data.firstName,
            lastName: data.lastName,
            mobileNumber: data.mobileNumber,
            email: data.email,
            isAdmin: data.isAdmin,
            folderId: data.folderId,
            fileId: data.fileId,
            profileImage: data.profileImage,
            token: localStorage.getItem("token"),
            cart: data.cart,
          });
          setCheckUser(true);
        });
    }
    setLastPage("/products");
  }, [setLastPage, setCheckAdminUpdate, checkUser, setUser, setCheckUser]);

  function refreshProductList() {
    setCheckAdminUpdate(false);
    setUpdateOrderList(false);
    setCheckUser(false);
  }

  function checkout() {
    Swal.fire({
      title: "Checkout",
      html: paymentMode.html,
      confirmButtonText: paymentMode.confirmButtonText,
      focusConfirm: false,
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((res) => res.json())
          .then((result) => {
            if (result.isSuccess) {
              Swal.fire({
                title: "Congratulations!",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: `Thank you for your payment.`,
              });
            } else {
              Swal.fire({
                title: "We're sorry.",
                icon: "error",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: `Something went wrong, try again later.`,
              });
            }

            setUpdateOrderList(false);
            setCheckAdminUpdate(false);
            setCheckUser(false);
          });
      }
    });
  }

  function checkPaymentMode(e) {
    if (e.target.value === "1") {
      setPaymentMode({
        confirmButtonText: "Confirm",
        html: CreditDebit,
      });
    } else if (e.target.value === "2") {
      setPaymentMode({
        confirmButtonText: "Check out with Paypal",
        html: Paypal,
      });
    } else if (e.target.value === "3") {
      setPaymentMode({
        confirmButtonText: "Check out with Gcash",
        html: GCash,
      });
    } else if (e.target.value === "4") {
      setPaymentMode({
        confirmButtonText: "Confirm COD",
        html: Cod,
      });
    }
  }

  return (
    <>
      <div className="d-md-none mb-3 mx-3">
        <Search />
      </div>
      <Container fluid className="text-center adminStyle py-3 mb-3">
        <h1 className="mb-3 text-gray">Your Shopping Cart</h1>
        <Card className="shadow-soft">
          <Container
            fluid
            style={{
              overflowX: "auto",
              overflowY: "auto",
              margin: "0",
              minHeight: "45vh",
              maxHeight: "80vh",
            }}
            className="scrollEdit"
          >
            <Table striped className="rounded mt-2">
              {/* style={{ userSelect: "none" }}> */}
              <thead>
                <tr>
                  <th style={{ width: "calc(100%/5)" }} className="border-0">
                    Date{" "}
                    <IoRefreshCircleOutline
                      className="shadow-inset rounded"
                      style={{ cursor: "pointer" }}
                      onClick={() => refreshProductList()}
                    />
                  </th>
                  <th style={{ width: "calc(100%/5)" }} className="border-0">
                    Name
                  </th>
                  <th style={{ width: "calc(100%/10)" }} className="border-0">
                    Price
                  </th>
                  <th style={{ width: "calc(100%/10)" }} className="border-0">
                    Quantity
                  </th>
                  <th style={{ width: "calc(100%/8)" }} className="border-0">
                    Sub Total
                  </th>
                  <th style={{ width: "calc(100%/20)" }} className="border-0">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {totalAmount !== 0 ? (
                  orders
                ) : (
                  <tr>
                    <td colSpan={6}>your cart is empty.</td>
                  </tr>
                )}
                {totalAmount !== 0 ? (
                  <tr>
                    <th colSpan={4} className="text-right">
                      Total
                    </th>
                    <td colSpan={2}>{totalAmount}</td>
                  </tr>
                ) : (
                  <></>
                )}
              </tbody>
            </Table>
          </Container>
          {totalAmount === 0 ? (
            <></>
          ) : (
            <>
              <div className="form-group">
                <select
                  className="custom-select text-center m-0 p-0 mt-3"
                  style={{ width: "140px" }}
                  id="paymentMode"
                  onChange={(e) => checkPaymentMode(e)}
                >
                  <option value={1}>Credit/Debit</option>
                  <option value={2}>Paypal</option>
                  <option value={3}>GCash</option>
                  <option value={4}>COD</option>
                </select>
              </div>
              <div>
                <Button
                  className="mt-3 mb-4"
                  style={{ width: "200px" }}
                  onClick={() => checkout()}
                >
                  Checkout
                </Button>
              </div>
            </>
          )}
        </Card>
      </Container>
    </>
  );
}
