import React, { useEffect } from "react";
import { useContext } from "react";
import { Container } from "react-bootstrap";
import Products from "../components/Products";
import Search from "../components/Search";
import UserContext from "../UserContext";

function ProductsPage() {
  const { setUser, setLastPage, checkUser, setCheckUser, setCheckAdminUpdate } =
    useContext(UserContext);

  useEffect(() => {
    if (localStorage.getItem("token") !== null && !checkUser) {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            userId: data.userId,
            firstName: data.firstName,
            lastName: data.lastName,
            mobileNumber: data.mobileNumber,
            email: data.email,
            isAdmin: data.isAdmin,
            folderId: data.folderId,
            fileId: data.fileId,
            profileImage: data.profileImage,
            token: localStorage.getItem("token"),
            cart: data.cart,
          });
          setCheckUser(true);
        });
    }
    setLastPage("/products");
  }, [setLastPage, setCheckAdminUpdate, checkUser, setUser, setCheckUser]);

  return (
    <>
      <div className="d-md-none mb-3 mx-3">
        <Search />
      </div>
      <Container fluid className="text-center adminStyle py-3">
        <h1 className="mb-3 text-gray">Our Products</h1>
        <Products />
      </Container>
    </>
  );
}

export default ProductsPage;
