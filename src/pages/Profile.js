import { useContext, useEffect, useState } from "react";
import {
  Container,
  Card,
  Row,
  Col,
  Form,
  InputGroup,
  Button,
} from "react-bootstrap";
import Tilt from "react-parallax-tilt";
import { TbCloudUpload } from "react-icons/tb";
import { FaUser, FaEnvelope, FaMobileAlt, FaRegIdCard } from "react-icons/fa";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import { Navigate, useNavigate } from "react-router-dom";

export default function Profile() {
  const navigate = useNavigate();
  const { user, setUser, setCheckUser } = useContext(UserContext);
  const [disable, setDisable] = useState(false);
  const [values, setValues] = useState({
    userId: user.userId,
    firstName: user.firstName,
    lastName: user.lastName,
    mobileNumber: user.mobileNumber,
    email: user.email,
    folderId: user.folderId,
    fileId: user.fileId,
    profileImage: user.profileImage,
  });
  const inputs = [
    {
      id: 1,
      name: "userId",
      label: "User ID",
      disabled: "true",
      icon: FaRegIdCard,
    },
    {
      id: 2,
      name: "firstName",
      label: "First Name",
      pattern: `[a-zA-Z ]*`,
      disabled: "true",
      icon: FaUser,
    },
    {
      id: 3,
      name: "lastName",
      label: "Last Name",
      pattern: `[a-zA-Z ]*`,
      disabled: "true",
      icon: FaUser,
    },
    {
      id: 4,
      name: "mobileNumber",
      label: "Mobile Number",
      disabled: "true",
      icon: FaMobileAlt,
    },
    {
      id: 5,
      name: "email",
      label: "Email Address",
      disabled: "true",
      icon: FaEnvelope,
    },
  ];

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  useEffect(() => {
    if (user.userId !== null) {
      setDisable(true);
    }
    if (values.userId === null || typeof values.userId === "undefined") {
      navigate("/");
    }
  }, [setDisable, user.userId, navigate, values.userId]);

  function toggleDisable() {
    if (disable === true) {
      setDisable(false);
    } else setDisable(true);
  }

  function updateUser(e) {
    e.preventDefault();
    if (
      validateEmailAddress(values.email) &&
      validateMobileNumber(values.mobileNumber) &&
      validateName(values.firstName) &&
      validateName(values.lastName)
    ) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });

      if (!disable) {
        fetch(`${process.env.REACT_APP_API_URL}/users/update`, {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${user.token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            firstName: values.firstName,
            lastName: values.lastName,
            mobileNumber: values.mobileNumber,
            email: values.email,
            folderId: values.folderId,
            fileId: values.fileId,
            profileImage: values.profileImage,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            setUser({
              userId: values.userId,
              firstName: values.firstName,
              lastName: values.lastName,
              mobileNumber: values.mobileNumber,
              email: values.email,
              folderId: values.folderId,
              fileId: values.fileId,
              profileImage: values.profileImage,
            });
            setDisable(true);
            setCheckUser(false);
            Swal.fire({
              title: "Update successful!",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Your profile is updated!",
            });
          })
          .catch((e) => {
            Swal.fire({
              title: "Update failed",
              icon: "error",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Something went wrong. Please try again later.",
            });
          });
      } else {
        setCheckUser(false);
        Swal.fire({
          title: "Update failed",
          icon: "error",
          iconColor: "#44476a",
          background: "#e6e7ee",
          text: "Nothing to update. Please try again.",
        });
      }
    } else {
      setCheckUser(false);
      Swal.fire({
        title: "Update failed",
        icon: "error",
        iconColor: "#44476a",
        background: "#e6e7ee",
        text: "Check your info. Please try again.",
      });
    }
  }

  function changePassword() {
    Swal.fire({
      title: "Update Password",
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <label for="currentPassword" style="font-size:16px">Current Password</label>
                <input id="currentPassword" type="password" class="form-control mb-2" ><span></span>
                <label for="newPassword" style="font-size:16px">New Password</label>
                <input id="newPassword" type="password" class="form-control mb-2" >
                <label for="confirmNewPassword" style="font-size:16px">Confirm New Password</label>
                <input id="confirmNewPassword" type="password" class="form-control mb-2" >
                <input id="showPassword" type="checkbox" > 
                <label for="showPassword" style="font-size:16px">Show Password</label>
              </div>
                `,
      confirmButtonText: "Save",
      focusConfirm: false,
      didOpen: (e) => {
        const checkBox = Swal.getPopup().querySelector("#showPassword");
        const curPass = Swal.getPopup().querySelector("#currentPassword");
        const newPass = Swal.getPopup().querySelector("#newPassword");
        const curNewPass = Swal.getPopup().querySelector("#confirmNewPassword");

        checkBox.addEventListener("click", function () {
          if (curPass.type === "password") {
            curPass.type = "text";
            newPass.type = "text";
            curNewPass.type = "text";
          } else {
            curPass.type = "password";
            newPass.type = "password";
            curNewPass.type = "password";
          }
        });
      },
      preConfirm: () => {
        const password =
          Swal.getPopup().querySelector("#currentPassword").value;
        const newPassword = Swal.getPopup().querySelector("#newPassword").value;
        const confirmNewPassword = Swal.getPopup().querySelector(
          "#confirmNewPassword"
        ).value;
        if (
          !validatePassword(password) ||
          !validatePassword(newPassword) ||
          !validatePassword(confirmNewPassword) ||
          confirmNewPassword !== newPassword
        ) {
          Swal.showValidationMessage(`Please check your password`);
        }

        return {
          password: password,
          newPassword: newPassword,
        };
      },
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        fetch(`${process.env.REACT_APP_API_URL}/users/changePassword`, {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${user.token}`,
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            password: result.value.password,
            newPassword: result.value.newPassword,
          }),
        })
          .then((res) => res.json())
          .then((resultUpdate) => {
            if (resultUpdate.isSuccess) {
              Swal.fire({
                title: "Update successful!",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Your password is now updated!",
              });
            } else {
              Swal.fire({
                title: "Update failed!",
                icon: "error",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Something went wrong, try again later.",
              });
            }
          });
      }
    });
  }

  function uploadFile(e) {
    let file = e.target.files[0]; //the file
    let reader = new FileReader(); //this for convert to Base64
    reader.readAsDataURL(file); //start conversion...
    reader.onprogress = function (e) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
    };
    reader.onload = async function (e) {
      //.. once finished..
      var rawLog = reader.result.split(",")[1]; //extract only thee file data part
      var dataSend = {
        dataReq: {
          folderId: values.folderId,
          fileId: `${values.fileId}`,
          data: rawLog,
          name: file.name,
          type: file.type,
        },
        fname: "uploadProfilePicture",
      }; //preapre info to send to API
      let resultUpload = await fetch(
        process.env.REACT_APP_GOOGLE_URL, //your AppsScript URL
        {
          method: "POST",
          body: JSON.stringify(dataSend),
        }
      )
        .then((res) => res.json())
        .then((a) => {
          if (a !== null) {
            return {
              isSuccess: true,
              fileId: a.id,
              url: a.url,
            };
          } else {
            setCheckUser(false);
            Swal.fire({
              title: "Upload failed",
              icon: "error",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Something went wrong. Please try again later.",
            });
          }
        })
        .catch((e) => {
          Swal.fire({
            title: "Update failed",
            icon: "error",
            iconColor: "#44476a",
            background: "#e6e7ee",
            text: "Nothing to upload. Please try again.",
          });
        });
      let resultUpdate;
      if (resultUpload.isSuccess || typeof resultUpload != "undefined") {
        resultUpdate = await fetch(
          `${process.env.REACT_APP_API_URL}/users/update`,
          {
            method: "PUT",
            headers: {
              Authorization: `Bearer ${user.token}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              folderId: values.folderId,
              fileId: resultUpload.fileId,
              profileImage: resultUpload.url,
            }),
          }
        )
          .then((res) => res.json())
          .then((data) => {
            if (data.isSuccess) return true;
            else return false;
          })
          .catch((e) => {
            setCheckUser(false);
            Swal.fire({
              title: "Upload failed",
              icon: "error",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Something went wrong. Please try again later.",
            });
          });
      } else {
        setCheckUser(false);
        Swal.fire({
          title: "Upload failed",
          icon: "error",
          iconColor: "#44476a",
          background: "#e6e7ee",
          text: "Something went wrong. Please try again later.",
        });
      }

      if (resultUpload.isSuccess && resultUpdate) {
        setValues({
          userId: values.userId,
          firstName: values.firstName,
          lastName: values.lastName,
          mobileNumber: values.mobileNumber,
          email: values.email,
          folderId: values.folderId,
          fileId: resultUpload.fileId,
          profileImage: resultUpload.url,
        });
        setUser({
          userId: values.userId,
          firstName: values.firstName,
          lastName: values.lastName,
          mobileNumber: values.mobileNumber,
          email: values.email,
          folderId: values.folderId,
          fileId: resultUpload.fileId,
          profileImage: resultUpload.url,
          token: user.token,
          cart: user.cart,
        });
        setCheckUser(false);
        Swal.fire({
          title: "Upload successful!",
          icon: "success",
          iconColor: "#44476a",
          background: "#e6e7ee",
          text: "Your profile picture is updated!",
        });
      } else {
        setCheckUser(false);
        Swal.fire({
          title: "Upload failed",
          icon: "error",
          iconColor: "#44476a",
          background: "#e6e7ee",
          text: "Something went wrong. Please try again later.",
        });
      }
      resultUpdate = "";
      resultUpload = "";
    };
  }

  function validatePassword(input) {
    let regex =
      /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  function validateEmailAddress(input) {
    let regex = /[^\s@]+@[^\s@]+\.[^\s@]+/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  function validateMobileNumber(input) {
    let regex = /^\+?\d+$/;
    if (regex.test(input) && (input.length === 13 || input.length === 3)) {
      return true;
    } else {
      return false;
    }
  }

  function validateName(input) {
    let regex = /^[a-zA-Z ]*$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  return user.userId === null || typeof user.userId === "undefined" ? (
    <Navigate to="/login" />
  ) : (
    <Container className="min-vh-100 d-flex bg-primary justify-content-center align-items-center pb-4">
      <Card className="bg-primary shadow-soft border-light p-3">
        <Row className="justify-content-around">
          <Col
            md={5}
            lg={5}
            className="card text-center justify-content-center align-items-center bg-primary m-3"
            id="imgContainer"
          >
            <Tilt
              className="Tilt"
              style={{ width: "75%", cursor: "pointer", borderRadius: "50%" }}
              options={{ scale: 0.9 }}
            >
              <TbCloudUpload className="custom-file-label-2 " />
              <input
                type="file"
                accept="image/*"
                className="custom-file-input"
                id="customFile"
                onChange={uploadFile}
              />
              <img
                id="profilePicture"
                className="mb-3 rounded "
                src={values.profileImage}
                style={{ width: "100%", objectFit: "contain" }}
                alt="Profile"
              ></img>
            </Tilt>
          </Col>
          <Col md={6} lg={6} className="justify-content-center pb-5">
            <Card.Header className="text-center">
              <h4 className="h4">User's Profile</h4>
            </Card.Header>
            <Form onSubmit={(e) => updateUser(e)} className="">
              {inputs.map((input) => (
                <Form.Group className="py-2" key={input.id}>
                  <Form.Label>{input.label}</Form.Label>
                  <InputGroup>
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <input.icon />
                      </span>
                    </div>
                    <Form.Control
                      name={input.name}
                      type="text"
                      placeholder={input.placeholder}
                      value={values[input.name]}
                      onChange={onChange}
                      pattern={input.pattern}
                      disabled={
                        input.name === "userId" || input.name === "email"
                          ? true
                          : disable
                      }
                      autoComplete="true"
                    />
                  </InputGroup>
                </Form.Group>
              ))}
              <div className="d-flex mt-4">
                <Button
                  className="btn btn-block btn-primary m-2"
                  type="button"
                  onClick={toggleDisable}
                >
                  Edit
                </Button>
                <Button
                  className="btn btn-block btn-primary m-2"
                  type="submit"
                  id="submitBtn"
                >
                  Update
                </Button>
              </div>
            </Form>
            <div className="d-flex justify-content-center align-items-center mt-2">
              <span className="font-weight-normal mr-2">Change password?</span>
              <span
                className="font-weight-bold"
                onClick={changePassword}
                style={{ cursor: "pointer" }}
              >
                {" "}
                here
              </span>
            </div>
          </Col>
        </Row>
      </Card>
    </Container>
  );
}
