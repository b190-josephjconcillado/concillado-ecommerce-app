import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Logout() {
  const {
    user,
    setUser,
    unSetUser,
    setCheckUser,
    setCheckAdminUpdate,
    setHomePage,
  } = useContext(UserContext);

  useEffect(() => {
    if (user.token !== null) {
      setUser({
        userId: null,
        firstName: null,
        lastName: null,
        mobileNumber: null,
        email: null,
        isAdmin: null,
        folderId: null,
        fileId: null,
        profileImage: null,
        token: null,
        cart: null,
      });
      setCheckUser(false);
      setCheckAdminUpdate(false);
      setHomePage(true);

      unSetUser();
      Swal.fire({
        title: "Thank you!",
        icon: "success",
        iconColor: "#44476a",
        background: "#e6e7ee",
        text: "You are now logged out!",
      });
    }
  }, [
    user,
    setUser,
    unSetUser,
    setCheckUser,
    setCheckAdminUpdate,
    setHomePage,
  ]);

  return <Navigate to="/login" />;
}
