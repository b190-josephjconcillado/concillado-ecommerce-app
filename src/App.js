import "./App.css";
import Home from "./pages/Home";
import Login from "./pages/Login";
import NavbarApp from "./components/Navbar";
import Register from "./pages/Register";
import { useRef, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import ErrorPage from "./pages/404";
import { UserProvider } from "./UserContext";
import "animate.css";
import Logout from "./pages/Logout";
import Profile from "./pages/Profile";
import ProductsPage from "./pages/ProductsPage";
import ProductView from "./components/ProductView";
import Admin from "./components/Admin";
import ResetPassword from "./pages/ResetPassword";
import OrderHistory from "./pages/OrderHistory";
import Cart from "./pages/Cart";
import Footer from "./components/Footer";
import About from "./pages/About";

function App() {
  const [user, setUser] = useState({
    userId: null,
    firstName: null,
    lastName: null,
    mobileNumber: null,
    email: null,
    isAdmin: null,
    folderId: null,
    fileId: null,
    profileImage: null,
    token: null,
    cart: null,
  });

  const [checkUser, setCheckUser] = useState(false);
  const [homePage, setHomePage] = useState(true);
  const [cart, setCart] = useState(0);
  const [lastPage, setLastPage] = useState("/");
  const [checkAdminUpdate, setCheckAdminUpdate] = useState(false);
  const [updateOrderList, setUpdateOrderList] = useState(false);
  const [navbarDropdown, setNavbarDropdown] = useState(false);
  const dropDownRefBtn = useRef();

  const unSetUser = () => {
    localStorage.clear();
    setHomePage(true);
  };

  function toggleDropHide(e) {
    if (
      e.target.id !== "searchBar" &&
      e.target.id !== "exampleInputIcon2" &&
      e.target.id !== "navbarScrollingDropdown" &&
      e.target.id !== "searchBtn" &&
      navbarDropdown
    ) {
      setNavbarDropdown(!navbarDropdown);
      dropDownRefBtn.current.click();
    }
  }

  return (
    <UserProvider
      value={{
        user,
        setUser,
        unSetUser,
        checkUser,
        setCheckUser,
        homePage,
        setHomePage,
        cart,
        setCart,
        lastPage,
        setLastPage,
        checkAdminUpdate,
        setCheckAdminUpdate,
        updateOrderList,
        setUpdateOrderList,
        navbarDropdown,
        setNavbarDropdown,
        dropDownRefBtn,
      }}
    >
      <Router>
        <Container fluid onClick={(e) => toggleDropHide(e)} className="m-0 p-0">
          <NavbarApp />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<ProductsPage />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/reset/:resetToken" element={<ResetPassword />} />
            <Route path="/orders" element={<OrderHistory />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/about" element={<About />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
          <Footer />
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
