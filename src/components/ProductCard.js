import React, { useContext, useEffect, useState } from "react";
import { Card, Col, Button } from "react-bootstrap";
import { BsCartPlus } from "react-icons/bs";
import UserContext from "../UserContext";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import Tilt from "react-parallax-tilt";

export default function ProductCard({ productProp }) {
  const { user, homePage, setCheckAdminUpdate, setLastPage, setCheckUser } =
    useContext(UserContext);
  const navigate = useNavigate();
  const { productName, price, _id, description, productImage } = productProp;
  const [winSize, setWinSize] = useState(window.innerWidth);

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setWinSize(window.innerWidth);
    }, 500);

    window.addEventListener("resize", debouncedHandleResize);

    return (_) => {
      window.removeEventListener("resize", debouncedHandleResize);
    };
  }, []);

  function addToCart() {
    if (user.userId === null) {
      Swal.fire({
        title: "Opps!",
        icon: "question",
        iconColor: "#44476a",
        background: "#e6e7ee",
        text: "You need to login before you can add items to cart.",
      });
      setLastPage("/products");
      navigate("/login");
    } else {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(`${process.env.REACT_APP_API_URL}/users/order/${_id}/1`, {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((result) => {
          if (result.isSuccess) {
            Swal.fire({
              title: "Congratulations!",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Successfully added to cart.",
            });
            setCheckAdminUpdate(false);
            setCheckUser(false);
          }
        });
    }
  }
  function loading() {
    setCheckUser(false);
    Swal.fire({
      allowEscapeKey: false,
      allowOutsideClick: false,
      showConfirmButton: false,
      customClass: {
        popup: "my-swal",
        loader: "my-swal-loader",
      },
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }
  function showPicture() {
    Swal.fire({
      title: `${productName}`,
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <div style="width:100%;height:300px;" class="card shadow-inset rounded mb-2">
                  <img class="rounded p-2" src="${productImage.link}" id="preview" alt="Preview Image" style="height:100%;width:100%;object-fit:contain" />
                </div>
              </div>
                `,
      confirmButtonText: "Okay",
      showDenyButton: false,
      focusConfirm: false,
    });
  }

  function debounce(fn, ms) {
    let timer;
    return (_) => {
      clearTimeout(timer);
      timer = setTimeout((_) => {
        timer = null;
        fn.apply(this, arguments);
      }, ms);
    };
  }

  return (
    <>
      <Col
        md={6}
        lg={3}
        xl={winSize >= 1600 ? 2 : 3}
        className="d-flex justify-content-between align-items-around mb-3"
      >
        <Card
          className="d-flex justify-content-between align-items-around bg-primary shadow-soft border-light m-2 p-3"
          id="products"
        >
          <Card
            className="mb-2 shadow-inset d-flex justify-content-center align-items-center"
            style={{ width: "100%", height: "220px", maxHeight: "280px" }}
          >
            <Tilt
              className="my-2"
              style={{ width: "100%", maxHeight: "200px" }}
              options={{ scale: 0.9 }}
            >
              <img
                style={{
                  objectFit: "contain",
                  width: "100%",
                  height: "100%",
                  cursor: "pointer",
                }}
                className="rounded mb-2 p-1"
                src={productImage.link}
                alt={_id}
                onClick={showPicture}
              />
            </Tilt>
          </Card>
          <Link to={`/products/${_id}`} className="h5" onClick={loading}>
            {productName}
          </Link>
          {homePage === false ? (
            <h3 className="h6 font-weight-light text-gray mt-2">
              {description}
            </h3>
          ) : (
            <div>
              <Link
                to={`/products/${_id}`}
                className="bg-primary badge badge-dark"
              >
                View details
              </Link>
            </div>
          )}
          <div className="d-flex justify-content-between align-items-center mt-3">
            <span className="h6 mb-0 text-gray">
              ₱{numberWithCommas(price)}
            </span>
            {!user.isAdmin ? (
              <Button
                className="btn btn-xs btn-primary mb-0"
                onClick={addToCart}
              >
                <BsCartPlus className="mr-2 mb-1" />
                Add to cart
              </Button>
            ) : (
              <></>
            )}
          </div>
        </Card>
      </Col>
    </>
  );
}
