import React, { useContext, useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

import ProductCard from "./ProductCard";

function Products() {
  const { homePage } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    Swal.fire({
      allowEscapeKey: false,
      allowOutsideClick: false,
      showConfirmButton: false,
      // backdrop: "rgba(255, 255, 255, 0.0)",
      customClass: {
        popup: "my-swal",
        loader: "my-swal-loader",
      },
      willOpen: () => {
        Swal.showLoading();
      },
    });
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        if (homePage) data = data.slice(0, 6);
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
        Swal.close();
      });
  }, [homePage]);

  return (
    <Container fluid>
      <Row>{products}</Row>
    </Container>
  );
}

export default Products;
