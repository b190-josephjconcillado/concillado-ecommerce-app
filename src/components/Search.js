import { useEffect, useRef, useState } from "react";
import { Nav, InputGroup } from "react-bootstrap";
import { FaSearch } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

export default function Search() {
  const [products, setProducts] = useState([]);
  const [query, setQuery] = useState("");
  const [selected, setSelected] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState("");
  const navigate = useNavigate();
  const searchResultRef = useRef();
  const inputRef = useRef();

  useEffect(() => {
    if (query !== "" && !selected)
      searchResultRef.current.style.visibility = "visible";
    else searchResultRef.current.style.visibility = "hidden";
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      });
  }, [query, selected]);

  function setSelectedValue(e, productId) {
    inputRef.current.focus();
    setSelected(true);
    setQuery(e.target.innerHTML);
    setSelectedProduct(productId);
  }

  function goToProduct() {
    if (query !== "") {
      setQuery("");
      setSelectedProduct("");
      setSelected(false);
      searchResultRef.current.style.visibility = "hidden";
      navigate(`/products/${selectedProduct}`);
    }
  }

  function onKeyDownInput(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      navigate(`/products/${selectedProduct}`);
      setQuery("");
      setSelectedProduct("");
      setSelected(false);
    }
    // if (e.key === "ArrowDown") {
    //   searchResultRef.current.focus();
    //   console.log(e.key);
    // }
  }

  return (
    <InputGroup className="input-group">
      <input
        id="searchBar"
        className="form-control"
        placeholder="search"
        aria-label="Input group"
        type="text"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        onClick={(e) => setSelected(false)}
        onKeyDown={(e) => onKeyDownInput(e)}
        ref={inputRef}
      />
      <div className="input-group-append">
        <span className="input-group-text input-group-append">
          <Nav.Link className="p-0 m-0" onClick={() => goToProduct()}>
            <FaSearch />
          </Nav.Link>
        </span>
      </div>
      <ul
        ref={searchResultRef}
        id="results"
        className="list-group w-100 rounded p-2 shadow-soft"
        style={{
          position: "absolute",
          marginTop: "50px",
          zIndex: "9999",
          backgroundColor: "#e6e7ee",
          visibility: "hidden",
        }}
      >
        {products ? (
          products
            .filter((prod) =>
              prod.productName.toLowerCase().includes(query.toLowerCase())
            )
            .map((product) => {
              return (
                <li
                  id="searchBtn"
                  type="button"
                  key={product._id}
                  className="m-1 border-0"
                  onClick={(e) => setSelectedValue(e, product._id)}
                >
                  {product.productName}
                </li>
              );
            })
        ) : (
          <>
            <li id="searchBtn" className="m-1 border-0">
              did not match any entries
            </li>
          </>
        )}
      </ul>
    </InputGroup>
  );
}
