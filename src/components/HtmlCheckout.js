module.exports.CreditDebit = `
<div class="form-group px-3 pt-3 px-md-5" >
    <h4 class="border-bottom border-gray p-1">Billing details - Credit/Debit</h4>
    <label for="address" style="font-size:14px">Address</label>
    <input id="address" type="text" class="form-control mb-2" placeholder='e.g. Purok 3, Brgy. ' required>
    <label for="city" style="font-size:14px">City</label>
    <input id="city" type="text" class="form-control mb-2" placeholder='e.g. Ormoc City' required>
    <label for="postalCode" style="font-size:14px">Zip/Postal Code</label>
    <input id="postalCode" type="text" class="form-control mb-2" placeholder='e.g. 6541' required>
    <label for="email" style="font-size:14px">Email</label>
    <input id="email" type="email" class="form-control mb-2" placeholder='e.g. email@example.com' required>
    <label for="mobileNumber" style="font-size:14px">Phone/Mobile Number</label>
    <input id="mobileNumber" type="number" class="form-control mb-2" placeholder='e.g. +639270000000'>
    <h4 class="border-bottom border-gray p-1">Card details</h4>
    <label for="name" style="font-size:14px">(Full name as displayed on card)</label>
    <input id="name" type="text" class="form-control mb-2" placeholder='Name on card ' required>
    <label for="cardNumber" style="font-size:14px">Card Number</label>
    <input id="cardNumber" type="text" class="form-control mb-2" placeholder='0000 0000 0000 0000'>
    <div class="d-flex justify-content-around">
        <label for="cvc" style="font-size:14px">CVC</label>
        <label for="cardExpiry" style="font-size:14px">Card Expiry</label>
    </div>
    <div class="d-flex justify-content-between">
        <input id="cvc" type="text" class="form-control text-center mr-1 mb-2" placeholder="CVC" required>
        <input id="cardExpiry" type="text" class="form-control text-center ml-1 mb-2" placeholder="MM/YY" required>
    </div>
</div>
`;

module.exports.Paypal = `
<div class="form-group px-3 pt-3 px-md-5" >
    <h4 class="border-bottom border-gray p-1">Billing details - Paypal</h4>
    <label for="name" style="font-size:14px">Full Name</label>
    <input id="name" type="text" class="form-control mb-2" placeholder='e.g. John Doe' required>
    <label for="email" style="font-size:14px">Email</label>
    <input id="email" type="email" class="form-control mb-2" placeholder='e.g. email@example.com' required>
</div>
`;

module.exports.GCash = `
<div class="form-group px-3 pt-3 px-md-5" >
    <h4 class="border-bottom border-gray p-1">Billing details - GCash</h4>
    <label for="name" style="font-size:14px">Full Name</label>
    <input id="name" type="text" class="form-control mb-2" placeholder='e.g. John Doe' required>
    <label for="mobileNumber" style="font-size:14px">Mobile Number</label>
    <input id="mobileNumber" type="number" class="form-control mb-2" placeholder='e.g. +639270000000'>
</div>
`;

module.exports.Cod = `
<div class="form-group px-3 pt-3 px-md-5" >
    <h4 class="border-bottom border-gray p-1">Billing details - COD</h4>
    <label for="address" style="font-size:14px">Address</label>
    <input id="address" type="text" class="form-control mb-2" placeholder='e.g. Purok 3, Brgy. ' required>
    <label for="city" style="font-size:14px">City</label>
    <input id="city" type="text" class="form-control mb-2" placeholder='e.g. Ormoc City' required>
    <label for="postalCode" style="font-size:14px">Zip/Postal Code</label>
    <input id="postalCode" type="text" class="form-control mb-2" placeholder='e.g. 6541' required>
    <label for="email" style="font-size:14px">Email</label>
    <input id="email" type="email" class="form-control mb-2" placeholder='e.g. email@example.com' required>
    <label for="mobileNumber" style="font-size:14px">Phone/Mobile Number</label>
    <input id="mobileNumber" type="number" class="form-control mb-2" placeholder='e.g. +639270000000'>
</div>
`;
