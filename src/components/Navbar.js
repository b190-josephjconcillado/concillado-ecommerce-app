import { Nav, Navbar, Container, Row, Col, NavDropdown } from "react-bootstrap";
import { FaTimes } from "react-icons/fa";
import { MdOutlineShoppingCart } from "react-icons/md";
import Badge from "@mui/material/Badge";
import Tilt from "react-parallax-tilt";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import Search from "./Search";

export default function NavbarComponent(prop) {
  const {
    user,
    setHomePage,
    setCheckUser,
    setCheckAdminUpdate,
    setUpdateOrderList,
    setNavbarDropdown,
    dropDownRefBtn,
  } = useContext(UserContext);
  const navigate = useNavigate();

  function logOut() {
    Swal.fire({
      title: "Are you sure you want to logout?",
      confirmButtonText: "Yes",
      showDenyButton: true,
      denyButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        setTimeout(() => {
          navigate("/logout");
        }, 2000);
      }
    });
  }
  function toHome() {
    setHomePage(true);
    setCheckAdminUpdate(false);
    setCheckUser(false);
  }
  function resetState() {
    setCheckAdminUpdate(false);
    setCheckUser(false);
    setUpdateOrderList(false);
    setHomePage(false);
  }

  function toggleDropBtn() {
    setNavbarDropdown(true);
  }

  return (
    <Navbar collapseOnSelect expand="md" variant="light">
      <Container fluid>
        <Navbar.Brand
          className="shadow-soft rounded border border-light"
          as={Link}
          to="/"
          onClick={toHome}
        >
          <Tilt className="p-1 tilt" perspective={300} scale={2}>
            <img
              id="logo"
              src="https://lh3.googleusercontent.com/d/1dQZ6HE_WGOirZe8BqtiOhG6GsHPAdmr7"
              alt="dodoflorie"
            ></img>
          </Tilt>
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="responsive-navbar-nav"
          className="shadow-inset rounded border border-light"
          ref={dropDownRefBtn}
          onClick={() => toggleDropBtn()}
        />
        <Navbar.Collapse id="navbar-default-primary">
          <Container
            fluid
            className="d-md-flex justify-content-between align-items-center"
          >
            <Nav className="navbar-collapse-header">
              <Row>
                <Col xs={6} className="collapse-brand">
                  <Navbar.Brand
                    className="shadow-soft rounded border border-light"
                    as={Link}
                    to="/"
                  >
                    <Tilt className="p-1 tilt" perspective={300} scale={2}>
                      <img
                        id="logo"
                        src="https://lh3.googleusercontent.com/d/1dQZ6HE_WGOirZe8BqtiOhG6GsHPAdmr7"
                        alt="dodoflorie"
                      ></img>
                    </Tilt>
                  </Navbar.Brand>
                </Col>
                <Col xs={6} className="collapse-close">
                  <Navbar.Toggle>
                    <FaTimes />
                  </Navbar.Toggle>
                </Col>
              </Row>
            </Nav>
            <Nav className="d-md-none align-items-center my-3 my-md-0 mx-md-3">
              <Search />
            </Nav>
            <Nav className="mr-3">
              <Nav.Link
                className="mx-lg-3"
                as={NavLink}
                to="/products"
                onClick={resetState}
              >
                Products
              </Nav.Link>
              <Nav.Link as={NavLink} to="/about">
                About
              </Nav.Link>
            </Nav>
            <Nav className="d-none d-md-flex my-3 my-md-0 mr-md-3 w-50">
              <Search />
            </Nav>
            <Nav className="d-md-flex align-items-md-center ml-md-2">
              {user.userId !== null || typeof user.userId === "undefined" ? (
                <Fragment>
                  <img
                    style={{
                      height: "35px",
                      width: "35px",
                      borderRadius: "50%",
                      objectFit: "cover",
                      cursor: "pointer",
                    }}
                    id="profilePicture"
                    htmlFor="collasible-nav-dropdown"
                    className="d-none d-md-inline align-items-center border-gray"
                    src={user.profileImage}
                    alt="profile pic"
                    onClick={() => navigate("/profile")}
                  ></img>
                  <NavDropdown
                    title="Account"
                    id="navbarScrollingDropdown"
                    // onClick={resetState}
                  >
                    <NavDropdown.Item
                      as={NavLink}
                      to="/profile"
                      onClick={resetState}
                    >
                      Profile
                    </NavDropdown.Item>
                    {user.isAdmin ? (
                      <></>
                    ) : (
                      <NavDropdown.Item
                        as={NavLink}
                        to="/orders"
                        onClick={resetState}
                      >
                        Orders
                      </NavDropdown.Item>
                    )}
                  </NavDropdown>
                  <Nav.Link onClick={logOut} className="mx-md-2">
                    Logout
                  </Nav.Link>
                </Fragment>
              ) : (
                <Fragment>
                  <Nav.Link as={NavLink} to="/login" className="mx-md-2">
                    Login
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/register" className="mx-md-2">
                    Register
                  </Nav.Link>
                </Fragment>
              )}
              {user.userId !== null || typeof user.userId === "undefined" ? (
                !user.isAdmin ? (
                  <Nav.Link
                    style={{ fontSize: "30px" }}
                    as={NavLink}
                    to="/cart"
                    onClick={resetState}
                  >
                    <Badge
                      sx={{
                        "& .MuiBadge-badge": {
                          backgroundColor: "#44476A",
                          color: "#e6e7ee",
                        },
                      }}
                      badgeContent={user.cart}
                      showZero
                    >
                      <MdOutlineShoppingCart />
                    </Badge>
                  </Nav.Link>
                ) : (
                  <></>
                )
              ) : (
                <></>
              )}
            </Nav>
          </Container>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
