import { Fragment, useState } from "react";
import { Button, Collapse, Table } from "react-bootstrap";
import { IoIosArrowDropdown, IoIosArrowDropup } from "react-icons/io";

export default function OrderPending({ orderPendingProp }) {
  const { userId, email, pendingOrders } = orderPendingProp;
  const [open, setOpen] = useState(false);

  return (
    <Fragment>
      <tr>
        <td
          className="d-flex justify-content-center align-items-between border-0"
          onClick={() => setOpen(!open)}
          style={{ cursor: "pointer" }}
        >
          <Button
            className="btn-xs mx-1 p-0 shadow-inset"
            aria-controls="example-collapse-text"
            aria-expanded={open}
          >
            {!open ? (
              <IoIosArrowDropdown style={{ fontSize: "20px" }} />
            ) : (
              <IoIosArrowDropup style={{ fontSize: "20px" }} />
            )}
          </Button>
          {userId}
        </td>
        <td className="border-0">{email}</td>
        <td className="border-0">₱{pendingOrders.totalAmount}</td>
      </tr>
      <Collapse in={open}>
        <tr id="example-collapse-text">
          <td colSpan={7} className="py-1">
            <Table style={{ fontSize: "13px" }}>
              <thead>
                <tr>
                  <th>ProductId</th>
                  <th>Date</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Sub Total</th>
                </tr>
              </thead>
              <tbody>
                {pendingOrders.pendingOrderProducts.length > 0 ? (
                  pendingOrders.pendingOrderProducts.map((product) => (
                    <tr key={product.productId + product.transactionDate}>
                      <td>{product.productId}</td>
                      <td>{product.transactionDate}</td>
                      <td>{product.name}</td>
                      <td>₱{product.price}</td>
                      <td>{product.quantity}</td>
                      <td>₱{product.price * product.quantity}</td>
                    </tr>
                  ))
                ) : (
                  <>
                    <tr>
                      <td colSpan={7}>There are no pending orders</td>
                    </tr>
                  </>
                )}
              </tbody>
            </Table>
          </td>
        </tr>
      </Collapse>
    </Fragment>
  );
}
