import React, { Fragment, useContext, useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { FaPlus } from "react-icons/fa";
import UserContext from "../../UserContext";
import Product from "./Product";
import Swal from "sweetalert2";
import { IoRefreshCircleOutline } from "react-icons/io5";

export default function ProductLists() {
  const { user, checkAdminUpdate, setCheckAdminUpdate } =
    useContext(UserContext);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    if (!checkAdminUpdate) {
      fetch(`${process.env.REACT_APP_API_URL}/products/allByAdmin`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setCheckAdminUpdate(true);
          setProducts(
            data.map((product) => {
              return <Product key={product._id} productProp={product} />;
            })
          );
        });
    }
  }, [checkAdminUpdate, setCheckAdminUpdate]);

  function addProduct() {
    Swal.fire({
      title: "Add Product",
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <div style="width:100%;height:300px;" class="card shadow-inset rounded mb-2">
                  <img class="border rounded p-2" src="https://lh3.googleusercontent.com/d/1clYtqGszanMrw0PRb--JGSQ3hPMaIWiB" id="preview" alt="Preview Image" style="height:100%;width:100%;object-fit:contain" />
                </div>
                <label for="name" style="font-size:16px">Name</label>
                <input id="name" type="text" class="form-control text-center mb-2" placeholder="">
                <label for="description" style="font-size:16px">Description</label>
                <textarea class="form-control text-center  mb-2" id="description" rows="3"></textarea>
                <div class="d-flex justify-content-around">
                  <label for="price" style="font-size:16px">Price</label>
                  <label for="stock" style="font-size:16px">Stock</label>
                </div>
                <div class="d-flex justify-content-between">
                  <input id="price" type="text" class="form-control text-center mr-1 mb-2" placeholder="">
                  <input id="stock" type="text" class="form-control text-center ml-1 mb-2" placeholder="">
                </div>
                <div>
                  <label for="customFile" style="font-size:16px">Upload Picture</label>
                </div>
                <div class="custom-file text-left mb-3" style="width:250px !important;">
                  <input type="file" accept="image/*" class="custom-file-input" id="customFile">
                  <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
              </div>
                `,
      confirmButtonText: "Save",
      showDenyButton: true,
      denyButtonText: "No",
      focusConfirm: false,
      didOpen: (e) => {
        const file = Swal.getPopup().querySelector("#customFile");
        const inputName = Swal.getPopup().querySelector("#name");
        const end = inputName.value.length;
        inputName.setSelectionRange(end, end);
        inputName.focus();
        file.addEventListener("change", function () {
          Swal.getPopup().querySelector("#preview").src = URL.createObjectURL(
            file.files[0]
          );
        });
      },
      preConfirm: () => {
        const nameVal = Swal.getPopup().querySelector("#name").value;
        const descriptionVal =
          Swal.getPopup().querySelector("#description").value;
        const priceVal = Swal.getPopup().querySelector("#price").value;
        const stockVal = Swal.getPopup().querySelector("#stock").value;
        const file = Swal.getPopup().querySelector("#customFile").files[0];

        return {
          productName: nameVal,
          description: descriptionVal,
          price: priceVal,
          quantity: stockVal,
          file: file,
        };
      },
    }).then(async (result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });

        if (typeof result.value.file !== "undefined") {
          let file = result.value.file;
          let reader = new FileReader(); //this for convert to Base64
          reader.readAsDataURL(file); //start conversion...
          reader.onload = async function (e) {
            var rawLog = reader.result.split(",")[1]; //extract only thee file data part
            var dataSend = {
              dataReq: {
                folderId: "1rZ-ZBPruRNV0iwqD_9SMYfaiJGtNk72f",
                fileId: "",
                data: rawLog,
                name: file.name,
                type: file.type,
              },
              fname: "uploadProductPicture",
            }; //preapre info to send to API

            let resultUpload = await fetch(
              process.env.REACT_APP_GOOGLE_URL, //your AppsScript URL
              {
                method: "POST",
                body: JSON.stringify(dataSend),
              }
            )
              .then((res) => res.json())
              .then((a) => {
                if (a !== null) {
                  return {
                    isSuccess: true,
                    fileId: a.id,
                    link: a.url,
                  };
                } else {
                  return {
                    isSuccess: false,
                  };
                }
              })
              .catch((e) => {
                console.log(e);
              });
            let resultCreate;

            if (resultUpload.isSuccess) {
              resultCreate = await fetch(
                `${process.env.REACT_APP_API_URL}/products/`,
                {
                  method: "POST",
                  headers: {
                    Authorization: `Bearer ${user.token}`,
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                    productName: result.value.productName,
                    description: result.value.description,
                    price: result.value.price,
                    quantity: result.value.quantity,
                    folderId: "1rZ-ZBPruRNV0iwqD_9SMYfaiJGtNk72f",
                    fileId: resultUpload.fileId,
                    link: resultUpload.link,
                  }),
                }
              )
                .then((res) => res.json())
                .then((data) => {
                  if (data.isSuccess) return true;
                  else return false;
                });
            }
            if (resultUpload.isSuccess && resultCreate) {
              setCheckAdminUpdate(false);
              Swal.fire({
                title: "Success",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Product added!",
              });
            } else {
              setCheckAdminUpdate(false);
              Swal.fire({
                title: "Update failed",
                icon: "error",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Something went wrong. Please try again later.",
              });
            }
            setCheckAdminUpdate(false);
            resultCreate = "";
            resultUpload = "";
          };
        } else {
          let data = {
            dataReq: {
              folderId: "1rZ-ZBPruRNV0iwqD_9SMYfaiJGtNk72f",
            },
            fname: "createProductImage",
          }; //preapre info to send to API

          let resultCreate = await fetch(
            process.env.REACT_APP_GOOGLE_URL, //your AppsScript URL
            {
              method: "POST",
              body: JSON.stringify(data),
            }
          )
            .then((res) => res.json())
            .then((a) => {
              if (a !== null) {
                return {
                  isSuccess: true,
                  fileId: a.id,
                  link: a.url,
                };
              } else {
                return {
                  isSuccess: false,
                };
              }
            });
          let resultProduct;

          if (resultCreate.isSuccess) {
            resultProduct = await fetch(
              `${process.env.REACT_APP_API_URL}/products/`,
              {
                method: "POST",
                headers: {
                  Authorization: `Bearer ${user.token}`,
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  productName: result.value.productName,
                  description: result.value.description,
                  price: result.value.price,
                  quantity: result.value.quantity,
                  folderId: "1rZ-ZBPruRNV0iwqD_9SMYfaiJGtNk72f",
                  fileId: resultCreate.id,
                  link: resultCreate.link,
                }),
              }
            )
              .then((ress) => ress.json())
              .then((data) => {
                if (data.isSuccess) {
                  return true;
                } else {
                  return false;
                }
              });
          }

          if (resultCreate.isSuccess && resultProduct) {
            setCheckAdminUpdate(false);
            Swal.fire({
              title: "Success",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Product added!",
            });
          } else {
            setCheckAdminUpdate(false);
            Swal.fire({
              title: "Failed",
              icon: "error",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Something went wrong. Please try again later.",
            });
          }

          resultCreate = "";
          resultProduct = "";
        }
        setCheckAdminUpdate(false);
      }
    });
  }

  function refreshProductList() {
    setCheckAdminUpdate(false);
  }

  return (
    <Fragment>
      <Container
        fluid
        style={{
          overflowX: "auto",
          overflowY: "auto",
          margin: "0",
          maxHeight: "60vh",
          minHeight: "35vh",
        }}
        className="scrollEdit"
      >
        <Table striped className="rounded mt-3">
          {/* style={{ userSelect: "none" }}> */}
          <thead>
            <tr>
              <th style={{ width: "calc(100%/5)" }} className="border-0">
                ProductID{" "}
                <IoRefreshCircleOutline
                  className="shadow-inset rounded"
                  style={{ cursor: "pointer" }}
                  onClick={() => refreshProductList()}
                />
              </th>
              <th style={{ width: "calc(100%/5)" }} className="border-0">
                Name
              </th>
              <th style={{ width: "calc(100%/10)" }} className="border-0">
                Price
              </th>
              <th style={{ width: "calc(100%/10)" }} className="border-0">
                Stock
              </th>
              <th style={{ width: "calc(100%/10)" }} className="border-0">
                Active
              </th>
              <th style={{ width: "calc(100%/2.5)" }} className="border-0">
                Action
              </th>
            </tr>
          </thead>
          <tbody>{products}</tbody>
        </Table>
      </Container>
      <Button className="my-4" onClick={addProduct}>
        <FaPlus className="d-none d-lg-inline mr-2 mb-1" />
        Add product
      </Button>
    </Fragment>
  );
}
