import { Tab, Tabs } from "react-bootstrap";
import OrderListPending from "./OrderListPending";
import OrderListPurchased from "./OrderListPurchased";

export default function OrderLists() {
  return (
    <div className="p-1 m-0">
      <Tabs
        defaultActiveKey="pending-list"
        id="tab"
        className="bg-primary border-light justify-content-around pt-2 mx-1"
      >
        <Tab
          eventKey="pending-list"
          title="Pending Orders"
          className="shadow-inset mx-1 mb-1 rounded"
        >
          <OrderListPending />
        </Tab>
        <Tab
          eventKey="purchased-list"
          title="Purchased Orders"
          className="shadow-inset mx-1 mb-1 rounded"
        >
          <OrderListPurchased />
        </Tab>
      </Tabs>
    </div>
  );
}
