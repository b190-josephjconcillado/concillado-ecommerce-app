import React, { Fragment, useContext, useEffect, useState } from "react";
import { Button, Container, Table } from "react-bootstrap";
import { FaUserPlus } from "react-icons/fa";
import User from "./User";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";
export default function UserLists() {
  const [users, setUsers] = useState([]);
  const { checkAdminUpdate, setCheckAdminUpdate, setUpdateOrderList } =
    useContext(UserContext);

  function validatePassword(input) {
    let regex =
      /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  function addUser() {
    Swal.fire({
      title: "Add New User",
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <label for="firstName" style="font-size:16px">First Name</label>
                <input id="firstName" type="text" class="form-control mb-2" placeholder='first name'>
                <label for="lastName" style="font-size:16px">Last Name</label>
                <input id="lastName" type="text" class="form-control mb-2" placeholder='last name'>
                <label for="mobileNumber" style="font-size:16px">Mobile Number</label>
                <input id="mobileNumber" type="text" class="form-control mb-2" placeholder='mobile number'>
                <label for="email" style="font-size:16px">Email</label>
                <input id="email" type="email" class="form-control mb-2" placeholder='email'>
                <label for="password" style="font-size:16px">Password</label>
                <input id="password" type="password" class="form-control mb-2" placeholder='password'>
                <label for="confirmPassword" style="font-size:16px">Confirm Password</label>
                <input id="confirmPassword" type="password" class="form-control mb-2" placeholder='confirm password'>
                <input id="swal2-checkbox" type="checkbox" > 
                <label for="swal2-checkbox" style="font-size:16px">Show Password</label>
              </div>
            `,
      confirmButtonText: "Yes",
      showDenyButton: true,
      denyButtonText: "No",
      focusConfirm: false,
      didOpen: (e) => {
        const checkBox = Swal.getPopup().querySelector("#swal2-checkbox");
        const password = Swal.getPopup().querySelector("#password");
        const conPass = Swal.getPopup().querySelector("#confirmPassword");

        let clickOnce = false;

        checkBox.addEventListener("click", function () {
          if (clickOnce === false) {
            password.type = "text";
            conPass.type = "text";
            clickOnce = true;
          } else {
            password.type = "password";
            conPass.type = "password";
            clickOnce = false;
          }
        });
      },
      preConfirm: () => {
        const valfn = Swal.getPopup().querySelector("#firstName").value;
        const valln = Swal.getPopup().querySelector("#lastName").value;
        const valmn = Swal.getPopup().querySelector("#mobileNumber").value;
        const valp = Swal.getPopup().querySelector("#password").value;
        const valcp = Swal.getPopup().querySelector("#confirmPassword").value;
        const vale = Swal.getPopup().querySelector("#email").value;
        if (
          !validatePassword(valp) ||
          !validatePassword(valcp) ||
          valp !== valcp ||
          !validateName(valfn) ||
          !validateName(valln) ||
          !validateMobileNumber(valmn) ||
          !validateEmailAddress(vale)
        ) {
          Swal.showValidationMessage(`Please enter correct details`);
        }
        return {
          firstName: valfn,
          lastName: valln,
          mobileNumber: valmn,
          email: vale,
          password: valp,
        };
      },
    }).then(async (result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        let checkUser = await fetch(
          `${process.env.REACT_APP_API_URL}/users/check`,
          {
            method: "POST",
            headers: {
              "Content-type": "application/json",
            },
            body: JSON.stringify({
              email: result.value.email,
            }),
          }
        )
          .then((res) => res.json())
          .then((resultCheck) => {
            return resultCheck.isSuccess;
          });

        if (checkUser) {
          let folderId = await fetch(process.env.REACT_APP_GOOGLE_URL, {
            method: "POST",
            body: JSON.stringify({
              dataReq: {
                email: result.value.email,
                folderId: process.env.REACT_APP_GOOGLE_FOLDER_ID,
              },
              fname: "createUserFolder",
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              return data.id;
            });

          if (folderId !== null || typeof folderId !== "undefined") {
            let regResult = await fetch(
              `${process.env.REACT_APP_API_URL}/users/register`,
              {
                method: "POST",
                headers: {
                  "Content-type": "application/json",
                },
                body: JSON.stringify({
                  firstName: result.value.firstName,
                  lastName: result.value.lastName,
                  mobileNumber: result.value.mobileNumber,
                  email: result.value.email,
                  password: result.value.password,
                  folderId: folderId,
                }),
              }
            )
              .then((res) => res.json())
              .then((resultReg) => {
                return resultReg.isSuccess;
              });

            if (regResult) {
              setCheckAdminUpdate(false);
              Swal.fire({
                title: "Success",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "New user added.",
              });
            } else {
              setCheckAdminUpdate(false);
              Swal.fire({
                title: "failed!",
                icon: "error",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Something went wrong.",
              });
            }
          }
        } else {
          setCheckAdminUpdate(false);
          Swal.fire({
            title: "Duplicate email found.",
            icon: "error",
            iconColor: "#44476a",
            background: "#e6e7ee",
            text: "Please provide different email.",
          });
        }
      }
    });
  }

  function validateEmailAddress(input) {
    let regex = /[^\s@]+@[^\s@]+\.[^\s@]+/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  function validateMobileNumber(input) {
    let regex = /^\+?\d+$/;
    if (regex.test(input) && (input.length === 13 || input.length === 3)) {
      return true;
    } else {
      return false;
    }
  }

  function validateName(input) {
    let regex = /^[a-zA-Z ]*$/;
    if (regex.test(input)) {
      return true;
    } else {
      return false;
    }
  }

  useEffect(() => {
    if (!checkAdminUpdate) {
      setUpdateOrderList(false);
      fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.isSuccess) {
            setCheckAdminUpdate(true);
            setUsers(
              data.result.map((user) => {
                return <User key={user.userId} userProp={user} />;
              })
            );
          }
        });
    }
  }, [checkAdminUpdate, setCheckAdminUpdate, setUpdateOrderList]);

  return (
    <Fragment>
      <Container
        fluid
        style={{
          overflowX: "auto",
          overflowY: "auto",
          margin: "0",
          maxHeight: "60vh",
          minHeight: "35vh",
        }}
        className="scrollEdit"
      >
        <Table striped className="rounded mt-3" style={{ userSelect: "none" }}>
          <thead>
            <tr>
              <th style={{ width: "calc(100%/4)" }} className="border-0">
                UserID
              </th>
              <th style={{ width: "calc(100%/4)" }} className="border-0">
                Email
              </th>
              <th style={{ width: "calc(100%/4)" }} className="border-0">
                Admin
              </th>
              <th style={{ width: "calc(100%/4)" }} className="border-0">
                Action
              </th>
            </tr>
          </thead>
          <tbody>{users}</tbody>
        </Table>
      </Container>
      <Button className="my-4" onClick={addUser}>
        <FaUserPlus className="d-none d-lg-inline mr-2 mb-1" />
        Add user
      </Button>
    </Fragment>
  );
}
