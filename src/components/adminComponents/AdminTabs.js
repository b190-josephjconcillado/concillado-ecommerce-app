import { Tab, Tabs } from "react-bootstrap";
import OrderLists from "./OrderLists";
// import { useNavigate } from "react-router-dom";
import ProductLists from "./ProductLists";
import UserLists from "./UserLists";
// import UserContext from "../../UserContext";
// import { useContext } from "react";
export default function AdminTabs() {
  // const navigate = useNavigate();
  // const { setCheckAdminUpdate } = useContext(UserContext);

  return (
    <Tabs
      defaultActiveKey="user-list"
      id="tab"
      className="bg-primary border-light justify-content-around pt-2 mx-3"
    >
      <Tab eventKey="user-list" title="Users" className="mb-3">
        <UserLists />
      </Tab>
      <Tab eventKey="product-list" title="Products" className="mb-3">
        <ProductLists />
      </Tab>
      <Tab eventKey="order-list" title="Orders" className="mb-3">
        <OrderLists />
      </Tab>
    </Tabs>
  );
}
