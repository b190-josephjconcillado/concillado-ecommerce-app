import { Fragment, useContext, useState } from "react";
import { Button, Collapse, Table } from "react-bootstrap";
import { FaUserCheck, FaUserTimes } from "react-icons/fa";
import UserContext from "../../UserContext";
import Swal from "sweetalert2";
import { IoIosArrowDropdown, IoIosArrowDropup } from "react-icons/io";

export default function User({ userProp }) {
  const { user, setCheckAdminUpdate } = useContext(UserContext);
  const {
    userId,
    email,
    firstName,
    lastName,
    mobileNumber,
    isAdmin,
    profileImage,
  } = userProp;
  const [open, setOpen] = useState(false);

  function promoteToggle() {
    Swal.fire({
      title: "Are you sure?",
      confirmButtonText: "Yes",
      showDenyButton: true,
      denyButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/promote`, {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${user.token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            isAdmin: !isAdmin,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            setCheckAdminUpdate(false);
            Swal.fire({
              title: "Success!",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "User successfully updated",
            });
          });
      }
    });
  }

  function showPicture() {
    Swal.fire({
      title: `${firstName}`,
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <div style="width:100%;height:500px;" class="card shadow-inset rounded mb-2">
                  <img class="rounded p-2" src="${profileImage}" id="preview" alt="Preview Image" style="height:100%;width:100%;object-fit:contain" />
                </div>
              </div>
                `,
      confirmButtonText: "Okay",
      showDenyButton: false,
      focusConfirm: false,
    });
  }

  return (
    <Fragment>
      <tr>
        <td
          className="d-flex justify-content-center align-items-between border-0"
          onClick={() => setOpen(!open)}
          style={{ cursor: "pointer" }}
        >
          <Button
            className="btn-xs mx-1 p-0 shadow-inset"
            aria-controls="example-collapse-text"
            aria-expanded={open}
          >
            {!open ? (
              <IoIosArrowDropdown style={{ fontSize: "20px" }} />
            ) : (
              <IoIosArrowDropup style={{ fontSize: "20px" }} />
            )}
          </Button>
          {userId}
        </td>
        <td className="border-0">{email}</td>
        <td className="border-0">
          {isAdmin ? (
            <span className="badge badge-success text-uppercase">yes</span>
          ) : (
            <span className="badge badge-danger text-uppercase">no</span>
          )}
        </td>
        {email === "admin@admin.com" ? (
          <td className="border-0">owner</td>
        ) : isAdmin === true ? (
          user.userId === userId ? (
            <td className="border-0">logged in</td>
          ) : (
            <td className="d-flex justify-content-center align-items-center border-0">
              <Button className="btn-xs mx-1 mb-0" onClick={promoteToggle}>
                <FaUserTimes className="d-none d-lg-inline mr-2 mb-1" />
                Demote
              </Button>
            </td>
          )
        ) : (
          <td className="d-flex justify-content-center align-items-center border-0">
            <Button className="btn-xs mx-1 mb-0" onClick={promoteToggle}>
              <FaUserCheck className="d-none d-lg-inline mr-2 mb-1" />
              Promote
            </Button>
          </td>
        )}
      </tr>
      <Collapse in={open}>
        <tr id="example-collapse-text">
          <td colSpan={4} className="py-1">
            <Table style={{ fontSize: "13px" }}>
              <thead>
                <tr>
                  <th>Picture</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Mobile Number</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th className="border-0">
                    <img
                      style={{
                        height: "35px",
                        width: "35px",
                        objectFit: "cover",
                        cursor: "pointer",
                      }}
                      className="border-gray"
                      src={profileImage}
                      onClick={() => showPicture()}
                      alt="profile pic"
                    ></img>
                  </th>
                  <td className="border-0">{firstName}</td>
                  <td className="border-0">{lastName}</td>
                  <td className="border-0">{mobileNumber}</td>
                </tr>
              </tbody>
            </Table>
          </td>
        </tr>
      </Collapse>
    </Fragment>
  );
}
