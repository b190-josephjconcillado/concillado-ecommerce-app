import { Fragment, useContext, useState } from "react";
import { Button, Collapse, Table } from "react-bootstrap";
import { FaCheck, FaRegEdit, FaTimes } from "react-icons/fa";
import UserContext from "../../UserContext";
import Swal from "sweetalert2";
import { IoIosArrowDropdown, IoIosArrowDropup } from "react-icons/io";

export default function Product({ productProp }) {
  const [open, setOpen] = useState(false);
  const { user, setCheckAdminUpdate } = useContext(UserContext);
  const {
    _id,
    productName,
    description,
    price,
    quantity,
    isActive,
    productImage,
  } = productProp;

  function activeToggle() {
    Swal.fire({
      title: "Are you sure?",
      confirmButtonText: "Yes",
      showDenyButton: true,
      denyButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${user.token}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            isActive: !isActive,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            setCheckAdminUpdate(false);
            Swal.fire({
              title: "Success!",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Product successfully updated.",
            });
          });
      }
      setCheckAdminUpdate(false);
    });
  }

  function editProduct() {
    Swal.fire({
      title: "Product Update",
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <div style="width:100%;height:300px;" class="card shadow-inset rounded mb-2">
                  <img class="rounded p-2" src="${productImage.link}" id="preview" alt="Preview Image" style="height:100%;width:100%;object-fit:contain" />
                </div>
                <label for="name" style="font-size:16px">Name</label>
                <input id="name" type="text" class="form-control text-center mb-2" value="${productName}">
                <label for="description" style="font-size:16px">Description</label>
                <textarea class="form-control text-center  mb-2" id="description" rows="3">${description}</textarea>
                <div class="d-flex justify-content-around">
                  <label for="price" style="font-size:16px">Price</label>
                  <label for="stock" style="font-size:16px">Stock</label>
                </div>
                <div class="d-flex justify-content-between">
                  <input id="price" type="text" class="form-control text-center mr-1 mb-2" value="${price}">
                  <input id="stock" type="text" class="form-control text-center ml-1 mb-2" value="${quantity}">
                </div>
                <div>
                  <label for="customFile" style="font-size:16px">Upload Picture</label>
                </div>
                <div class="custom-file text-left mb-3" style="width:250px !important;">
                  <input type="file" accept="image/*" class="custom-file-input" id="customFile">
                  <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
              </div>
                `,
      confirmButtonText: "Save",
      showDenyButton: true,
      denyButtonText: "No",
      focusConfirm: false,
      didOpen: (e) => {
        const file = Swal.getPopup().querySelector("#customFile");
        const inputName = Swal.getPopup().querySelector("#name");
        const end = inputName.value.length;
        inputName.setSelectionRange(end, end);
        inputName.focus();
        file.addEventListener("change", function () {
          Swal.getPopup().querySelector("#preview").src = URL.createObjectURL(
            file.files[0]
          );
        });
      },
      preConfirm: () => {
        const nameVal = Swal.getPopup().querySelector("#name").value;
        const descriptionVal =
          Swal.getPopup().querySelector("#description").value;
        const priceVal = Swal.getPopup().querySelector("#price").value;
        const stockVal = Swal.getPopup().querySelector("#stock").value;
        const file = Swal.getPopup().querySelector("#customFile").files[0];

        return {
          productName: nameVal,
          description: descriptionVal,
          price: priceVal,
          quantity: stockVal,
          file: file,
        };
      },
    }).then(async (result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });

        if (typeof result.value.file !== "undefined") {
          let file = result.value.file;
          let reader = new FileReader(); //this for convert to Base64
          reader.readAsDataURL(file); //start conversion...
          reader.onload = async function (e) {
            var rawLog = reader.result.split(",")[1]; //extract only thee file data part
            var dataSend = {
              dataReq: {
                folderId: productImage.folderId,
                fileId: productImage.fileId,
                data: rawLog,
                name: file.name,
                type: file.type,
              },
              fname: "uploadProductPicture",
            }; //preapre info to send to API

            let resultUpload = await fetch(
              process.env.REACT_APP_GOOGLE_URL, //your AppsScript URL
              {
                method: "POST",
                body: JSON.stringify(dataSend),
              }
            )
              .then((res) => res.json())
              .then((a) => {
                if (a !== null) {
                  return {
                    isSuccess: true,
                    fileId: a.id,
                    url: a.url,
                  };
                } else {
                  return {
                    isSuccess: false,
                  };
                }
              })
              .catch((e) => {
                console.log(e);
              });
            let resultUpdate;

            if (resultUpload.isSuccess) {
              resultUpdate = await fetch(
                `${process.env.REACT_APP_API_URL}/products/${_id}`,
                {
                  method: "PUT",
                  headers: {
                    Authorization: `Bearer ${user.token}`,
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                    productName: result.value.productName,
                    description: result.value.description,
                    price: result.value.price,
                    quantity: result.value.quantity,
                    folderId: productImage.folderId,
                    fileId: resultUpload.fileId,
                    link: resultUpload.url,
                  }),
                }
              )
                .then((res) => res.json())
                .then((data) => {
                  if (data.isSuccess) return true;
                  else return false;
                })
                .catch((e) => {
                  console.log(e);
                });
            }
            if (resultUpload.isSuccess && resultUpdate) {
              setCheckAdminUpdate(false);
              Swal.fire({
                title: "Update successful!",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Product is updated!",
              });
            } else {
              setCheckAdminUpdate(false);
              Swal.fire({
                title: "Update failed",
                icon: "error",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: "Something went wrong. Please try again later.",
              });
            }
            setCheckAdminUpdate(false);
            resultUpdate = "";
            resultUpload = "";
          };
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
            method: "PUT",
            headers: {
              Authorization: `Bearer ${user.token}`,
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              productName: result.value.productName,
              description: result.value.description,
              price: result.value.price,
              quantity: result.value.quantity,
              folderId: productImage.folderId,
              fileId: productImage.fileId,
              link: productImage.link,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data.isSuccess) {
                setCheckAdminUpdate(false);
                Swal.fire({
                  title: "Update successful!",
                  icon: "success",
                  iconColor: "#44476a",
                  background: "#e6e7ee",
                  text: "Product is updated!",
                });
              } else {
                setCheckAdminUpdate(false);
                Swal.fire({
                  title: "Update failed",
                  icon: "error",
                  iconColor: "#44476a",
                  background: "#e6e7ee",
                  text: "Something went wrong. Please try again later.",
                });
              }
            })
            .catch((e) => {
              console.log(e);
            });
        }
        setCheckAdminUpdate(false);
      }
    });
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function showPicture() {
    Swal.fire({
      title: `${productName}`,
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <div style="width:100%;height:500px;" class="card shadow-inset rounded mb-2">
                  <img class="rounded p-2" src="${productImage.link}" id="preview" alt="Preview Image" style="height:100%;width:100%;object-fit:contain" />
                </div>
              </div>
                `,
      confirmButtonText: "Okay",
      showDenyButton: false,
      focusConfirm: false,
    });
  }

  return (
    <Fragment>
      <tr>
        <td
          className="d-flex justify-content-center align-items-center border-0 border-0"
          onClick={() => setOpen(!open)}
          style={{ cursor: "pointer" }}
        >
          <Button
            className="btn-xs mx-1 p-0 shadow-inset"
            aria-controls="example-collapse-text"
            aria-expanded={open}
          >
            {!open ? (
              <IoIosArrowDropdown style={{ fontSize: "20px" }} />
            ) : (
              <IoIosArrowDropup style={{ fontSize: "20px" }} />
            )}
          </Button>
          {_id}
        </td>
        <td className="border-0">{productName}</td>
        <td className="border-0">{numberWithCommas(price)}</td>
        <td className="border-0">{quantity}</td>
        <td className="border-0">
          {isActive ? (
            <span className="badge badge-success text-uppercase">yes</span>
          ) : (
            <span className="badge badge-danger text-uppercase">no</span>
          )}
        </td>

        <td className="border-0">
          {isActive === true ? (
            <Button className="btn-xs mx-1 mb-2" onClick={activeToggle}>
              <FaTimes className="d-none d-lg-inline mr-2 mb-1" />
              Deactivate
            </Button>
          ) : (
            <Button className="btn-xs mx-1 mb-2" onClick={activeToggle}>
              <FaCheck className="d-none d-lg-inline mr-2 mb-1" />
              Activate
            </Button>
          )}
          <Button className="btn-xs mx-1 mb-2" onClick={editProduct}>
            <FaRegEdit className="d-none d-lg-inline mr-2 mb-1" />
            Edit
          </Button>
        </td>
      </tr>
      <Collapse in={open}>
        <tr id="example-collapse-text">
          <td colSpan={6} className="py-1">
            <Table style={{ fontSize: "13px" }} className="border-0">
              <thead>
                <tr>
                  <th>Picture</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="border-0">
                    <img
                      style={{
                        height: "35px",
                        width: "35px",
                        objectFit: "cover",
                        cursor: "pointer",
                      }}
                      className="border-gray"
                      src={productImage.link}
                      onClick={() => showPicture()}
                      alt="profile pic"
                    ></img>
                  </td>
                  <td className="border-0 ">{description}</td>
                </tr>
              </tbody>
            </Table>
          </td>
        </tr>
      </Collapse>
    </Fragment>
  );
}
