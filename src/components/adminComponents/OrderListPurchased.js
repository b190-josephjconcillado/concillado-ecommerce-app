import { Fragment, useContext, useEffect, useState } from "react";
import { Container, Table } from "react-bootstrap";
import UserContext from "../../UserContext";
import OrderPurchased from "./OrderPurchased";
import { IoRefreshCircleOutline } from "react-icons/io5";

export default function OrderListPurchased() {
  const [orders, setOrders] = useState([]);
  const { setCheckAdminUpdate, updateOrderList, setUpdateOrderList } =
    useContext(UserContext);

  useEffect(() => {
    if (!updateOrderList) {
      fetch(`${process.env.REACT_APP_API_URL}/users/order/purchased`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setOrders(
            data.map((order) => {
              return (
                <OrderPurchased key={order.userId} orderPurchasedProp={order} />
              );
            })
          );
          setUpdateOrderList(true);
        });
    }
  }, [updateOrderList, setUpdateOrderList]);

  function refreshOrderListFunction() {
    setCheckAdminUpdate(false);
    setUpdateOrderList(false);
  }

  return (
    <Fragment>
      <Container
        fluid
        style={{
          overflowX: "auto",
          overflowY: "auto",
          margin: "0",
          minHeight: "35vh",
          maxHeight: "60vh",
        }}
        className="scrollEdit"
      >
        <Table striped>
          {/* style={{ userSelect: "none" }}> */}
          <thead>
            <tr>
              <th style={{ width: "calc(100%/4)" }} className="border-0">
                UserID{" "}
                <IoRefreshCircleOutline
                  className="shadow-inset rounded"
                  style={{ cursor: "pointer" }}
                  onClick={() => refreshOrderListFunction()}
                />
              </th>
              <th style={{ width: "calc(100%/4)" }} className="border-0">
                Email
              </th>
              <th style={{ width: "calc(100%/8)" }} className="border-0">
                Total
              </th>
            </tr>
          </thead>
          <tbody>{orders}</tbody>
        </Table>
      </Container>
      {/* </div> */}
    </Fragment>
  );
}
