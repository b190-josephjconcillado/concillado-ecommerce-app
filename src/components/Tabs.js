import { NavLink, Tab, Tabs } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Products from "./Products";
import Swal from "sweetalert2";
import { useContext } from "react";
import UserContext from "../UserContext";
import Tilt from "react-parallax-tilt";

export default function TabsApp() {
  const navigate = useNavigate();
  const { setHomePage, setCheckUser } = useContext(UserContext);
  function loading() {
    setCheckUser(false);
    setHomePage(false);
    navigate("/products");
    Swal.fire({
      allowEscapeKey: false,
      allowOutsideClick: false,
      showConfirmButton: false,
      customClass: {
        popup: "my-swal",
        loader: "my-swal-loader",
      },
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }
  return (
    <Tabs
      defaultActiveKey="general-merchandise"
      id="tab"
      className="bg-primary border-light justify-content-around pt-2 mx-3"
    >
      <Tab
        eventKey="general-merchandise"
        title="General Merchandise"
        className="mx-3"
      >
        <div className="text-center adminStyle mb-4 p-2">
          <h1 className="mb-0 text-gray">Featured Products</h1>
          <NavLink
            onClick={loading}
            className="bg-primary badge badge-dark mb-3"
          >
            View all products
          </NavLink>
          <Products />
        </div>
      </Tab>
      <Tab eventKey="eatry" title="Eatery" className="mx-3">
        <div
          className="text-center adminStyle mb-4 p-2"
          style={{ minHeight: "60vh", maxHeight: "80vh" }}
        >
          <h1 className="mb-0 text-gray">Under Construction</h1>
          <Tilt className="Tilt text-center" options={{ scale: 0.9 }}>
            <img
              style={{ height: "50vh", width: "100%", objectFit: "contain" }}
              src="https://lh3.googleusercontent.com/d/1jpPXn9baHTui24FClCycoWf6WhSFnHA1"
              alt="PNG"
            ></img>
          </Tilt>
        </div>
      </Tab>
      <Tab eventKey="rice-mill" title="Rice Mill" className="mx-3">
        <div
          className="text-center adminStyle mb-4 p-2"
          style={{ minHeight: "60vh", maxHeight: "80vh" }}
        >
          <h1 className="mb-0 text-gray">Under Construction</h1>
          <Tilt className="Tilt text-center" options={{ scale: 0.9 }}>
            <img
              style={{ height: "50vh", width: "100%", objectFit: "contain" }}
              src="https://lh3.googleusercontent.com/d/1jpPXn9baHTui24FClCycoWf6WhSFnHA1"
              alt="PNG"
            ></img>
          </Tilt>
        </div>
      </Tab>
    </Tabs>
  );
}
