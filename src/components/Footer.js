import { useContext } from "react";
import { Container, NavLink, OverlayTrigger, Tooltip } from "react-bootstrap";
import {
  FaFacebookF,
  FaFacebookMessenger,
  FaInstagram,
  FaTwitter,
} from "react-icons/fa";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Footer() {
  const { setCheckUser, setCheckAdminUpdate, setUpdateOrderList, setHomePage } =
    useContext(UserContext);
  function resetState() {
    setCheckAdminUpdate(false);
    setCheckUser(false);
    setUpdateOrderList(false);
    setHomePage(false);
  }

  function contactUs() {
    Swal.fire({
      title: "Contact Us",
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                  <label for="name" style="font-size:16px">Full Name</label>
                  <input id="name" type="text" class="form-control mb-2" placeholder="e.g. John Doe" require>
                  <label for="email" style="font-size:16px">Email</label>
                  <input id="email" type="email" class="form-control mb-2" placeholder="e.g. email@example.com" require>
                  <label for="message" style="font-size:16px">Message</label>
                  <textarea class="form-control  mb-2" id="message" rows="4" placeholder="e.g. Hi, how are you?" ></textarea>
                </div>
                  `,
      confirmButtonText: "Send",
      focusConfirm: false,
      preConfirm: () => {
        const nameVal = Swal.getPopup().querySelector("#name").value;
        const emailVal = Swal.getPopup().querySelector("#email").value;
        const messageVal = Swal.getPopup().querySelector("#message").value;

        if (nameVal === "" || messageVal === "" || emailVal === "")
          Swal.showValidationMessage(`Please, dont leave it blank, thank you.`);

        return {
          name: nameVal,
          email: emailVal,
          message: messageVal,
        };
      },
    }).then(async (result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        await fetch(`${process.env.REACT_APP_API_URL}/users/contact`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: result.value.name,
            email: result.value.email,
            message: result.value.message,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            if (data.accepted !== null) {
              Swal.fire({
                title: "Success",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: `Thank you ${result.value.name}. You will hear from us soon.`,
              });
            } else {
              Swal.fire({
                title: "Something went wrong",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: `Please, try again later ${result.value.name}.`,
              });
            }
          });
      }
    });
  }

  return (
    <footer className="bd-footer px-3 pt-5 px-md-5 mt-5 pb-2 bg-soft text-center">
      <Container>
        <Link to="/" target="_blank">
          <img
            src="https://lh3.googleusercontent.com/d/1dQZ6HE_WGOirZe8BqtiOhG6GsHPAdmr7"
            className="mb-2"
            width="50"
            alt="dodoflorie Logo"
          />
        </Link>
        <div className="d-flex justify-content-center align-items-center mb-2">
          <span className="font-small m-0">
            Designed and built by{" "}
            <NavLink
              href="https://josephjconcillado.ml"
              target="_blank"
              className="font-weight-bold d-inline p-0"
            >
              dre88mz
            </NavLink>
          </span>
        </div>
        <ul className="d-flex border-bottom border-gray p-2 justify-content-center list-unstyled mb-2">
          <li className="mr-2">
            <NavLink
              href="https://www.facebook.com/"
              target="_blank"
              className="btn btn-icon-only btn-pill btn-link"
              aria-label="facebook social link"
            >
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Like us on Facebook</Tooltip>}
              >
                <span>
                  <FaFacebookF aria-hidden="true" className="mb-1" />
                </span>
              </OverlayTrigger>
            </NavLink>
          </li>
          <li className="mr-2">
            <NavLink
              href="https://www.messenger.com/"
              target="_blank"
              className="btn btn-icon-only btn-pill btn-link"
              aria-label="Messenger social link"
            >
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Contact us in Messenger</Tooltip>}
              >
                <span>
                  <FaFacebookMessenger aria-hidden="true" className="mb-1" />
                </span>
              </OverlayTrigger>
            </NavLink>
          </li>
          <li className="mr-2">
            <NavLink
              href="https://www.instagram.com/"
              target="_blank"
              className="btn btn-icon-only btn-pill btn-link"
              aria-label="Instagram social link"
            >
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Follow us on Instagram</Tooltip>}
              >
                <span>
                  <FaInstagram aria-hidden="true" className="mb-1" />
                </span>
              </OverlayTrigger>
            </NavLink>
          </li>
          <li>
            <NavLink
              href="https://twitter.com/?lang=en"
              target="_blank"
              className="btn btn-icon-only btn-pill btn-link"
              aria-label="twitter social link"
            >
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip>Follow us on twitter</Tooltip>}
              >
                <span>
                  <FaTwitter aria-hidden="true" className="mb-1" />
                </span>
              </OverlayTrigger>
            </NavLink>
          </li>
        </ul>
        <ul className="bd-footer-links d-flex justify-content-center list-unstyled mb-1">
          <li className="mr-3">
            <Link to="/products" onClick={() => resetState()}>
              <p
                style={{ cursor: "pointer" }}
                className="font-weight-bold mb-0"
              >
                Products
              </p>
            </Link>
          </li>
          <li className="mr-3">
            <Link to="/about">
              <p
                style={{ cursor: "pointer" }}
                className="font-weight-bold mb-0"
              >
                About
              </p>
            </Link>
          </li>
          <li>
            <p
              style={{ cursor: "pointer" }}
              className="font-weight-bold mb-0"
              onClick={() => contactUs()}
            >
              Contact
            </p>
          </li>
        </ul>
      </Container>
    </footer>
  );
}
