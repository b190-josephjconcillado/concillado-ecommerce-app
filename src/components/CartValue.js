import { useContext, useEffect, useState } from "react";
import { Button, InputGroup } from "react-bootstrap";
import { FaTrashAlt } from "react-icons/fa";
import { FiMinus, FiPlus } from "react-icons/fi";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
export default function CartValue({ cartProp }) {
  const { productId, transactionDate, name, price, quantity } = cartProp;
  const [quantityValue, setQuantityValue] = useState(quantity);
  const { setCheckAdminUpdate, setCheckUser, setUpdateOrderList } =
    useContext(UserContext);
  const [checkStock, setCheckStock] = useState(false);
  const [currStock, setCurrStock] = useState(0);
  useEffect(() => {
    if (!checkStock) {
      fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setCurrStock(data.quantity);
          setCheckStock(true);
        });
    }
  });

  function decrement(val) {
    if (val >= 1) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(
        `${process.env.REACT_APP_API_URL}/users/order/remove/${productId}/1`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
        .then((res) => res.json())
        .then((result) => {
          if (result.isSuccess) {
            Swal.close();
            setUpdateOrderList(false);
            setCheckAdminUpdate(false);
            setCheckUser(false);
            setQuantityValue(val);
            setCheckStock(false);
          }
        });
    } else {
      setQuantityValue(1);
    }
  }

  function increment(val) {
    if (val <= currStock) {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(`${process.env.REACT_APP_API_URL}/users/order/${productId}/1`, {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((result) => {
          if (result.isSuccess) {
            Swal.close();
            setUpdateOrderList(false);
            setCheckAdminUpdate(false);
            setCheckUser(false);
            setQuantityValue(val);
            setCheckStock(false);
          }
        });
    } else {
      setQuantityValue(currStock);
    }
  }

  function remove() {
    Swal.fire({
      title: "Are you sure you?",
      confirmButtonText: "Yes",
      showDenyButton: true,
      denyButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton: false,
          customClass: {
            popup: "my-swal",
            loader: "my-swal-loader",
          },
          willOpen: () => {
            Swal.showLoading();
          },
        });
        fetch(
          `${process.env.REACT_APP_API_URL}/users/order/remove/${productId}/${quantityValue}`,
          {
            method: "PUT",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
          .then((res) => res.json())
          .then((result) => {
            if (result.isSuccess) {
              Swal.fire({
                title: "Success",
                icon: "success",
                iconColor: "#44476a",
                background: "#e6e7ee",
                text: `${name} is removed from the cart.`,
              });
              setUpdateOrderList(false);
              setCheckAdminUpdate(false);
              setCheckUser(false);
              setCheckStock(false);
            }
          });
      }
    });
  }

  return (
    <tr key={productId + transactionDate}>
      <td className="border-0">{transactionDate}</td>
      <td className="border-0">{name}</td>
      <td className="border-0">₱{price}</td>
      <td className="border-0">
        <div className="d-flex justify-content-around">
          <InputGroup id="quantityValue" className="w-50">
            <div className="input-group-prepend">
              <span
                className="input-group-text"
                style={{ cursor: "pointer" }}
                onClick={() => decrement(quantityValue - 1)}
              >
                <FiMinus />
              </span>
            </div>
            <label className="form-control">{quantityValue}</label>
            <div className="input-group-append">
              <span
                className="input-group-text"
                style={{ cursor: "pointer" }}
                onClick={() => increment(quantityValue + 1)}
              >
                <FiPlus />
              </span>
            </div>
          </InputGroup>
        </div>
      </td>
      <td className="border-0">₱{price * quantity}</td>
      <td className="border-0">
        <Button className="btn-xs m-0" onClick={() => remove()}>
          <FaTrashAlt />
        </Button>
      </td>
    </tr>
  );
}
