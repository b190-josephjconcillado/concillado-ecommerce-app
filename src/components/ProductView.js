import { useContext, useEffect, useState } from "react";
import { Container, Card, Button, Row, Col, InputGroup } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import { BsCartPlus } from "react-icons/bs";
import Tilt from "react-parallax-tilt";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import Search from "./Search";

import { FiMinus, FiPlus } from "react-icons/fi";

function ProductView() {
  const {
    user,
    setCheckUser,
    setUser,
    checkUser,
    setCheckAdminUpdate,
    setLastPage,
    setHomePage,
  } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();
  const [product, setProduct] = useState({
    productName: "",
    description: "",
    price: 0,
    quantity: 0,
    isActive: false,
    link: "",
    folderId: "",
  });

  const [quantityValue, setQuantityValue] = useState(1);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct({
          productName: data.productName,
          description: data.description,
          price: data.price,
          quantity: data.quantity,
          isActive: data.isActive,
          link: data.productImage.link,
          folderId: data.productImage.folderId,
        });
        Swal.close();
      });
  }, [productId]);

  useEffect(() => {
    if (localStorage.getItem("token") !== null && !checkUser) {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            userId: data.userId,
            firstName: data.firstName,
            lastName: data.lastName,
            mobileNumber: data.mobileNumber,
            email: data.email,
            isAdmin: data.isAdmin,
            folderId: data.folderId,
            fileId: data.fileId,
            profileImage: data.profileImage,
            token: localStorage.getItem("token"),
            cart: data.cart,
          });
          setCheckUser(true);
          Swal.close();
        });
    }
    setLastPage("/products");
  }, [setLastPage, setCheckAdminUpdate, checkUser, setUser, setCheckUser]);

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function toHome() {
    setCheckUser(false);
    setHomePage(false);
  }

  function addToCart() {
    if (user.userId === null) {
      Swal.fire({
        title: "Opps!",
        icon: "question",
        iconColor: "#44476a",
        background: "#e6e7ee",
        text: "You need to login before you can add items to cart.",
      });
      setLastPage("/products");
      navigate("/login");
    } else {
      Swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        customClass: {
          popup: "my-swal",
          loader: "my-swal-loader",
        },
        willOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(
        `${process.env.REACT_APP_API_URL}/users/order/${productId}/${quantityValue}`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
        .then((res) => res.json())
        .then((result) => {
          if (result.isSuccess) {
            Swal.fire({
              title: "Congratulations!",
              icon: "success",
              iconColor: "#44476a",
              background: "#e6e7ee",
              text: "Successfully added to cart.",
            });
            setCheckAdminUpdate(false);
            setCheckUser(false);
          }
        });
    }
  }
  function checkQuantityValue(val) {
    if (val < 1) return 1;
    else if (val >= product.quantity) return product.quantity;
    else return val;
  }

  function showPicture() {
    Swal.fire({
      title: `${product.productName}`,
      html: ` <div class="form-group px-3 pt-3 px-md-5" >
                <div style="width:100%;height:500px;" class="card shadow-inset rounded mb-2">
                  <img class="rounded p-2" src="${product.link}" id="preview" alt="Preview Image" style="height:100%;width:100%;object-fit:contain" />
                </div>
              </div>
                `,
      confirmButtonText: "Okay",
      showDenyButton: false,
      focusConfirm: false,
    });
  }

  return (
    <>
      <div className="d-md-none mx-3">
        <Search />
      </div>
      <Container className="min-vh-100 d-flex bg-primary justify-content-center align-items-center py-3">
        <Card className="bg-primary shadow-soft border-light p-3">
          <Row className="justify-content-around">
            <Col
              md={4}
              className="card text-center justify-content-center align-items-center bg-primary m-3"
              id="imgContainer"
            >
              <Card className="shadow-inset p-2">
                <Tilt
                  className="Tilt"
                  style={{ width: "100%", maxHeight: "250px" }}
                  options={{ scale: 0.9 }}
                >
                  <img
                    style={{
                      objectFit: "contain",
                      width: "100%",
                      height: "100%",
                      cursor: "pointer",
                    }}
                    className="rounded mb-2"
                    src={product.link}
                    alt={product.productName}
                    onClick={showPicture}
                  />
                </Tilt>
              </Card>
            </Col>
            <Col md={5} className="text-center justify-content-center p-2">
              <Card.Header className="text-center">
                <h4 className="h4">{product.productName}</h4>
              </Card.Header>
              <Card.Body className="mb-5">
                <h3 className="h6 font-weight-light text-gray">
                  {product.description}
                </h3>
                <div className="d-flex text-center justify-content-around align-items-center my-4">
                  <Card.Text className="h6 mb-0 text-gray">
                    Price: ₱{numberWithCommas(product.price)}
                  </Card.Text>
                  <Card.Text className="h6 mb-0 text-gray">
                    Available stock: {product.quantity}
                  </Card.Text>
                </div>
                <div className="d-flex justify-content-around">
                  {!user.isAdmin ? (
                    <InputGroup id="quantityValue" className="mt-3 w-50">
                      <div className="input-group-prepend">
                        <span
                          className="input-group-text"
                          style={{ cursor: "pointer" }}
                          onClick={() =>
                            setQuantityValue(
                              checkQuantityValue(quantityValue - 1)
                            )
                          }
                        >
                          <FiMinus />
                        </span>
                      </div>
                      <label className="form-control">{quantityValue}</label>
                      <div className="input-group-append">
                        <span
                          className="input-group-text"
                          style={{ cursor: "pointer" }}
                          onClick={() =>
                            setQuantityValue(
                              checkQuantityValue(quantityValue + 1)
                            )
                          }
                        >
                          <FiPlus />
                        </span>
                      </div>
                    </InputGroup>
                  ) : (
                    <></>
                  )}
                </div>
              </Card.Body>
              <div className="d-flex text-center justify-content-center align-items-center mt-3">
                {user.isAdmin ? (
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="font-weight-normal">
                      Add/Edit products
                      <Link
                        to="/"
                        onClick={toHome}
                        className="font-weight-bold"
                      >
                        {" "}
                        here
                      </Link>
                    </span>
                  </div>
                ) : (
                  <Button className="btn btn-primary mb-0" onClick={addToCart}>
                    <BsCartPlus className="mr-2 mb-1" />
                    Add to cart
                  </Button>
                )}
              </div>
              <div className="d-flex justify-content-center align-items-center mt-5">
                <span className="font-weight-normal">
                  See all products?
                  <Link
                    to="/products"
                    onClick={toHome}
                    className="font-weight-bold"
                  >
                    {" "}
                    click here
                  </Link>
                </span>
              </div>
            </Col>
          </Row>
        </Card>
      </Container>
    </>
  );
}

export default ProductView;
