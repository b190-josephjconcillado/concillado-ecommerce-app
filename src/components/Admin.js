import React from "react";
import { Container } from "react-bootstrap";
import AdminTabs from "./adminComponents/AdminTabs";

export default function Admin() {
  return (
    <Container fluid className="text-center adminStyle">
      <h1 className="text-gray">ADMIN DASHBOARD</h1>
      <AdminTabs />
    </Container>
  );
}
