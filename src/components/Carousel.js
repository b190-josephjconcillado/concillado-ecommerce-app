import { Carousel, Card, Container } from "react-bootstrap";
import {
  MdOutlineArrowBackIos,
  MdOutlineArrowForwardIos,
} from "react-icons/md";
// import Tilt from "react-parallax-tilt";

export default function CarouselApp() {
  const nextIcon = (
    <MdOutlineArrowForwardIos
      style={{ fontSize: "50px", color: "#44476A", opacity: "0.8" }}
    />
  );
  const prevIcon = (
    <MdOutlineArrowBackIos
      style={{ fontSize: "50px", color: "#44476A", opacity: "0.8" }}
    />
  );

  return (
    <Container
      fluid
      className="d-flex justify-content-center align-items-center pb-4 mt-3 mt-lg-0"
      style={{
        paddingRight: "0px !important",
        paddingLeft: "0px !important",
      }}
    >
      <Card
        className=""
        style={{
          width: "100vw",
        }}
      >
        <Carousel
          nextIcon={nextIcon}
          prevIcon={prevIcon}
          nextLabel=""
          prevLabel=""
          indicators="false"
          className="slide shadow-soft border border-light "
        >
          <Carousel.Item style={{ height: "50vh" }}>
            <img
              id="imgSlide"
              className="d-flex border border-light"
              src="https://lh3.googleusercontent.com/d/1jLNx7cj1silSYeyFRFTIMnV5vqwKJYr9"
              alt="First slide"
              style={{ height: "100%", width: "100vw", objectFit: "cover" }}
            />
            <Carousel.Caption>
              <h3>General Merchandise</h3>
              <p>
                Improvement retail corporation that sells tools, construction
                products, and other services
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item style={{ height: "50vh" }}>
            <img
              id="imgSlide"
              className="d-flex border border-light "
              src="https://lh3.googleusercontent.com/d/1jMLmMOOmX4cKvLjk2X1wYHUqimXwfpSm"
              alt="Second slide"
              style={{ height: "100%", width: "100vw", objectFit: "cover" }}
            />
            <Carousel.Caption>
              <h3>Eatery</h3>
              <p>Pagkain para sa masa. Makarelate bisag kinsa</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item style={{ height: "50vh" }}>
            <img
              id="imgSlide"
              className="d-flex border border-light "
              src="https://lh3.googleusercontent.com/d/1jElnvwNCMLhmNQAIj8OQlCA6Y1jxHesS"
              alt="Third slide"
              style={{ height: "100%", width: "100vw", objectFit: "cover" }}
            />
            <Carousel.Caption>
              <h3>Rice Mill</h3>
              <p>Built for quality polished rice.</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Card>
    </Container>
  );
}
